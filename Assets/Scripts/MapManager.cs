﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapManager : MonoBehaviour {

    public Text _txtCurPlanetName;

    public float m_fSlideTime;
    public float[] m_aryTheMainlandCenters;

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    public static MapManager s_Instance = null;

    public string[] m_aryDistrictName;

    public Sprite m_sprBg_Locked_Left;
    public Sprite m_sprBg_Locked_Right;
    public Sprite m_sprBgUnlocked_Left;
    public Sprite m_sprBgUnlocked_Right;

    ///// 各种金币显示UI
    public MoneyCounter[] m_aryMoneyCounters;
    public Image[] m_aryVariousCoinIcons;

    /// end 各种金币显示UI


    /// <summary>
    ///  UI
    /// </summary>
    public Button _btnAdventure;


    public GameObject _containerZengShouCounters;
    public GameObject _containerRecycledCounter;
    public GameObject _panelZengShou;

    public GameObject _panelPanelDetails;
    public Text _txtPanelDetailstitle;
    public UIDistrict[] m_aryUIDistricts;
    public UIPlanet[] m_aryUIPlanets;

    public GameObject _panelUnlockDistrict;
    public Text _txtUnlockDistrictCost;
    public Image _imgUnlockTrackPriceIcon;
      
    public GameObject _panelUnlockPlanet;
    public Text _txtUnlockPlanetCost;
    public Image _imgUnlockPlanetPriceIcon;

    public GameObject _panelEntering;

    public GameObject _containerPlanets;

    public GameObject _containerMainPlanets;
    public GameObject _containerOtherPlanets;

    public GameObject _panelScienceTree;


    public Text _txtCurPlanetAndTrack;
    public Text _txtCurTrackLevel;
    public GameObject[] m_aryPlanetTitle;


    // 离线收益相关
    public GameObject _subpanelBatCollectOffline;
    public Text _txtBatOfflineGain;
    public Text _txtBatOfflineDetail;
    public Text _txtBatOfflineTitle;
    //// end UI


    public GameObject _goStarSky;
    public float m_fStarSkyMoveSpeed = 1f;

    public enum ePlanetId
    {
        cooper,
        silver,
        gold,
        activity,
    };

    public enum ePlanetStatus
    {
        unlocked,        // 已解锁
        can_unlock,      // 未解锁，当前可以解锁
        can_not_unlock,  // 未解锁，当前不能解锁
    };

    public enum eDistrictStatus
    {
        unlocked,        // 已解锁
        can_unlock,      // 未解锁，当前可以解锁
        can_not_unlock,  // 未解锁，当前不能解锁
    };

    public Planet[] m_aryPlanets;

    public Planet m_CurPlanet = null;
    public District m_CurDistrict = null;

    public int m_nCurShowPlanetDetialIdOnUI = 0;

    Dictionary<string, UIZengShouCounter> m_dicZengShouCounters = new Dictionary<string, UIZengShouCounter>();

    public Text[] m_aryCoin0Value;
    public Text[] m_aryCoin1Value;
    public Text[] m_aryCoin2Value;
    public Text[] m_aryDiamondValue;

    private void Awake()
    {
        s_Instance = this;
    }

    private void FixedUpdate()
    {
        if ( ScienceTree.s_Instance.m_bShowingScienceTree )
        {
            return;
        }

        StarSkyMoveLoop();
        DoSliding();
        Dragging();
    }

    public void Init()
    {


    }

    public void LoadMyData_CurTrackPlanesData()
    {
        // 采取读档机制
        m_CurPlanet = null;// m_aryPlanets[0];
        m_CurDistrict = null;// m_CurPlanet.GetDistrictById(0);


        // 星球解锁情况
        for (int i = 0; i < DataManager.s_Instance.MAX_PLANET_NUM; i++)
        {
            Planet planet = m_aryPlanets[i];
            double dUnlock = (int)DataManager.s_Instance.GetMyData("PlanetUnlock" + i);
            if (dUnlock == 1)
            {
                planet.DoUnlock();
                Debug.Log("星球已解锁：" + i);
            }

            // 赛道解锁情况
            District[] aryTracks = planet.GetDistrictsList();
            for (int j = 0; j < DataManager.s_Instance.MAX_TRACK_NUM_OF_PLANET; j++)
            {
                District track = aryTracks[j];
                dUnlock = (int)DataManager.s_Instance.GetMyData("TrackUnlock" + i + "_" + j);
                if (dUnlock == 1)
                {
                    track.DoUnlock();
                    Debug.Log("赛道已解锁：" + i + "_" + j);
                }

            } // end j

        } // end for i

        string szData = DataManager.s_Instance.GetMyData_String("CurPlanetAndTrack");
        if ( szData == DataManager.LOAD_MY_DATA_INVALID_STRING )
        {
            // 如果没有存档，则默认进入星球0、赛道0
            EnterDistrict(0, 0);
            AccountSystem.s_Instance.InitMoney();
            return;
        }
        string[] aryParmas = szData.Split( '_' );
        int nPlanetId = int.Parse(aryParmas[0]);
        int nTrackId = int.Parse(aryParmas[1]);

        Debug.Log( "当前所处赛道:" + szData);

        EnterDistrict(nPlanetId, nTrackId);




        UpdateTrackInfo();
        AccountSystem.s_Instance.InitMoney();

    }



    // Use this for initialization
    void Start()
    {

     
    }

    public static bool IsTrackKeyValid(string szKey)
    {
        if (szKey == "")
        {
            return false;
        }
        return true;
    }

    public void NewZengShouCounter( int nPlanetId, int nDistrictId )
    {
        UIZengShouCounter zengshou_counter = ResourceManager.s_Instance.NewZengShouCounter().GetComponent<UIZengShouCounter>();
        //zengshou_counter.transform.SetParent(_containerZengShouCounters.transform);
        vecTempScale.x = 1f;
        vecTempScale.y = 1f;
        vecTempScale.z = 1f;
        zengshou_counter.transform.localScale = vecTempScale;
        zengshou_counter.SetPlanetIdAndDistrictId(nPlanetId, nDistrictId);

        m_dicZengShouCounters[nPlanetId + "_" + nDistrictId] = zengshou_counter;

     //   int nRate = ( nPlanetId + 1 ) * (nDistrictId + 1);

        float fRate = DataManager.s_Instance.GetTrackConfigById(nPlanetId, nDistrictId).fEarning;

        zengshou_counter._txtDistrictRaise.text = "x" + fRate/*nRate*/;
    }
	
	// Update is called once per frame
	void Update () {
        PreEnterRaceLoop();

        UpdatePlanetInfoLoop();

        BigMapLoop();
    }

    float m_fUpdatePlanetInfoLoopTimeElapse = 0;
    void UpdatePlanetInfoLoop()
    {
        if ( !m_bPlanetDetailPanelOpen )
        {
            return;
        }

        m_fUpdatePlanetInfoLoopTimeElapse += Time.deltaTime;
        if (m_fUpdatePlanetInfoLoopTimeElapse< 1f)
        {
            return;
        }
        m_fUpdatePlanetInfoLoopTimeElapse = 0;

        // right here
    UpdateUIDistrictsInfo();
    }


   
    public void UpdateTrackInfo()
    {
        if (m_CurDistrict == null || m_CurPlanet == null)
        {
            return;
        }
       
            _txtCurTrackLevel.text = "当前赛道等级：" + m_CurDistrict.GetLevel();
       
   
            _txtCurPlanetAndTrack.text = "星球:" + m_CurPlanet.GetId() + ", 赛道：" + (m_CurDistrict.GetId());


        AdministratorManager.s_Instance.UpdateAdminMainPanelInfo();
    }

    public void UnLockPlanet( int nPlanetId )
    {
        if ( nPlanetId == 0 )
        {
            //UIMsgBox.s_Instance.ShowMsg( "0号星球是起始星球，无需解锁" );
            return;
        }

        Planet planet_to_unlock = MapManager.s_Instance.GetPlanetById(nPlanetId);
        Planet planet_prev = MapManager.s_Instance.GetPlanetById(nPlanetId - 1);

       
        if (planet_prev.GetStatus() != ePlanetStatus.unlocked)
        {
            UIMsgBox.s_Instance.ShowMsg("先解锁上一级星球");
            return;
        }

        if (!planet_prev.CheckIfAllDistrictsUnlocked())
        {
            UIMsgBox.s_Instance.ShowMsg("先解锁上一星球所有赛道");
            return;
        }


        double nCoinCost = planet_to_unlock.GetUnlockCoinCost();
        double nCurTotalCoin = planet_prev.GetCoin();
        if (nCurTotalCoin < nCoinCost)
        {
            UIMsgBox.s_Instance.ShowMsg("上一级星球的金币数量不足:" + CyberTreeMath.GetFormatMoney( nCurTotalCoin ) );
            return;
        }

        // 解锁
        // planet_prev.CostCoin(nCoinCost);
        double nCurCoin = AccountSystem.s_Instance.GetCoin(planet_prev.GetId());
        AccountSystem.s_Instance.SetCoin(planet_prev.GetId(), nCurCoin - nCoinCost);
        planet_to_unlock.DoUnlock();


        UIMsgBox.s_Instance.ShowMsg( "星球解锁成功" );
        _panelUnlockPlanet.SetActive( false );
        UpdateUIPlanetsInfo();
    }

    public void UnLockDistrict(int nPlanetId, int nDistrictId)
    {
        Planet planet = GetPlanetById(nPlanetId);
        if ( planet.GetStatus() != ePlanetStatus.unlocked )
        {
            UIMsgBox.s_Instance.ShowMsg( "这个星球都还没解锁，谈不上赛道解锁" );
            return;
        }

        District district_to_unlock = planet.GetDistrictById(nDistrictId);
        if (nDistrictId == 0 || district_to_unlock.GetStatus() == eDistrictStatus.unlocked)
        {
            UIMsgBox.s_Instance.ShowMsg( "该赛道已解锁，无需再做此操作" );
            return;
        }

        District district_prev = planet.GetDistrictById(nDistrictId - 1);
        if (district_prev.GetStatus() != eDistrictStatus.unlocked)
        {
            UIMsgBox.s_Instance.ShowMsg( "先解锁上一级赛道" );
            return;
        }


        double nPriceToUnlock = district_to_unlock.GetUnlockPrice();
        double nCurTotalCoin = planet.GetCoin();
        if (nCurTotalCoin < nPriceToUnlock)
        {
            UIMsgBox.s_Instance.ShowMsg( "星球金币数量不足");
            return;
        }

        // 正式解锁
        //planet.CostCoin(nPriceToUnlock);
        double nCurCoint = AccountSystem.s_Instance.GetCoin(planet.GetId());
        AccountSystem.s_Instance.SetCoin(planet.GetId(),  nCurCoint - nPriceToUnlock);
        district_to_unlock.DoUnlock();

        UIMsgBox.s_Instance.ShowMsg( "赛道解锁成功" );
        _panelUnlockDistrict.SetActive(false);
        UpdateUIDistrictsInfo();



    }

    public void DoSomethingWhenChangeTrack()
    {
        UnlockNewPlaneManager.s_Instance.Reset();
    }

    public void EnterDistrict(int nPlanetId, int nDistrictId)
    {
        Debug.Log( "enter distrcit:" + nPlanetId + "_" + nDistrictId);
      


        Planet planet = MapManager.s_Instance.GetPlanetById(nPlanetId);
        if (planet == null) 
        {
            Debug.LogError("planet == null");
            return;
        }
        if (planet.GetStatus() != MapManager.ePlanetStatus.unlocked)
        {
            Debug.Log("星球" + (nPlanetId) + "尚未解锁");
            return;
        }

        Planet cur_planet = MapManager.s_Instance.GetCurPlanet();
        District cur_district = MapManager.s_Instance.GetCurDistrict();

        District district = planet.GetDistrictById(nDistrictId);
        if (district == null)
        {
            Debug.LogError("district == null");
        }

        if (district == cur_district)
        {
            Debug.Log("已经在这个赛道中了");
            return;

        }

        if (district.GetStatus() != MapManager.eDistrictStatus.unlocked)
        {
            Debug.Log("星球" + (nPlanetId) + "的赛道" + (nDistrictId) + "未解锁");
            return;
        }

        JTDHSceneManager.s_Instance.ChangeScene(nPlanetId); // 执行切换场景

        MapManager.s_Instance.SaveCurDistrictData();


        m_CurPlanet = planet;

        // 离开的这个District变成离线状态
        if (m_CurDistrict)
        {
            m_CurDistrict.OffLine();
        }
        if (m_CurDistrict != district)
        {
            DoSomethingWhenChangeTrack();
        }

        m_CurDistrict = district;
        m_CurDistrict.OnLine();

      

        Main.s_Instance.Load(m_CurPlanet, m_CurDistrict);

        Main.s_Instance._moneyCoin.SetValue(m_CurPlanet.GetCoin());
        // Main.s_Instance._txtPrestigeTimes.text = m_CurDistrict.GetPrestigeTimes().ToString();
        Main.s_Instance.SetPrestigeTimes(m_CurDistrict.GetPrestigeTimes());


        DebugInfo.s_Instance.SetPlanetAndDistrict(m_CurPlanet.GetId(), m_CurDistrict.GetId());

        Main.s_Instance.UpdateRaise();

        // poppin to do
        // Main.s_Instance.ShowCoin();

        SkillManager.s_Instance.UpdateSkillButtonsStatus();


        // 根据所处的星球，UI上的金币图标显示本星球独有的金币图标
        UpdateCoinIcon(m_CurPlanet.GetId());


        UpdateTrackInfo(  );

        // 存档：当前所在的星球和赛道
        DataManager.s_Instance.SaveMyData("CurPlanetAndTrack", m_CurPlanet.GetId() + "_" + m_CurDistrict.GetId());
    }

    public void UpdateCoinIcon( int nPlanetId )
    {
        Sprite spr = ResourceManager.s_Instance.GetCoinSpriteByPlanetId(nPlanetId);

        for (int i = 0; i < m_aryMoneyCounters.Length; i++ )
        {
            MoneyCounter counter = m_aryMoneyCounters[i];
            if (counter == null)
            {
                continue;
            }
            counter.SetIcon(spr);
        }

        for (int i = 0; i < m_aryVariousCoinIcons.Length; i++ )
        {
            Image imgIcon = m_aryVariousCoinIcons[i];
            if (imgIcon == null)
            {
                continue;
            }
            imgIcon.sprite = spr;
        }


    }



    public Planet GetPlanetById(int nId)
    {
        if (nId < 0 || nId >= m_aryPlanets.Length)
        {
            return null;
        }

        return m_aryPlanets[nId];

    }

    public Planet GetCurPlanet()
    {
        return m_CurPlanet;
    }

    public District GetCurDistrict()
    {
        return m_CurDistrict;
    }

    public void SaveCurDistrictData()
    {
        if (m_CurDistrict == null)
        {
            return;
        }

        string szDistrictData = Main.s_Instance.GenerateData();
        m_CurDistrict.SetData(szDistrictData);


    }

    public Planet m_PlanetToPrestige = null;
    public District m_DistrictToPrestige = null;

    public void OnClick_Prestige()
    {
        /*
        int nCurTotalCoin = m_CurPlanet.GetCoin();
        int nCost = DataManager.s_Instance.GetPrestigaeCoinCost(m_CurPlanet.GetId(), m_CurDistrict.GetId());
        if (nCurTotalCoin < nCost)
        {
            UIMsgBox.s_Instance.ShowMsg("金币不足");
            return;
        }

        m_CurPlanet.CostCoin(nCost);
        m_CurDistrict.DoPrestige(); // 注意，“重生”是针对赛道的，不是针对星球的
        */
        DoPrestige(m_PlanetToPrestige, m_DistrictToPrestige);

        Main.s_Instance._panelPrestige.SetActive( false );

        Main.s_Instance.UpdateRaise();
    }

    public void DoPrestige( Planet planet, District district )
    {
        double nCurTotalCoin = planet.GetCoin();
        int nCost = DataManager.s_Instance.GetPrestigaeCoinCost(planet.GetId(), district.GetId());
        if (nCurTotalCoin < nCost)
        {
            UIMsgBox.s_Instance.ShowMsg("金币不足");
            return;
        }

        //planet.CostCoin(nCost);
        double nCurCoin = AccountSystem.s_Instance.GetCoin(planet.GetId());
        AccountSystem.s_Instance.SetCoin(planet.GetId(), nCurCoin - nCost);
        district.DoPrestige(); // 注意，“重生”是针对赛道的，不是针对星球的
    }

    bool m_bPlanetDetailPanelOpen = false;
    public void OpenPlanetDetailPanel( int nPlanetId )
    {
        m_bPlanetDetailPanelOpen = true;

        m_nCurShowPlanetDetialIdOnUI = nPlanetId;

        _panelPanelDetails.SetActive( true );
        Planet cur_planet = GetPlanetById( nPlanetId );
        _txtPanelDetailstitle.text = GetPlanetNameById(nPlanetId);

        UpdateUIDistrictsInfo();
    }

    public void UpdateUIPlanetsInfo()
    {
        for (int i = 0; i <  m_aryPlanets.Length; i++ )
        {
            UIPlanet ui_planet = m_aryUIPlanets[i]; UIMainLand ui_main_land = m_aryMainLands[i];

            Planet planet = m_aryPlanets[i];
            if ( /*i > 0*/true)
            {
                ui_planet.SetUnlockPrice( planet.GetUnlockCoinCost() ); ui_main_land.SetUnlockPrice(planet.GetUnlockCoinCost(), i);
                ui_planet._txtPlanetName.text = DataManager.s_Instance.GetPlanetConfigById( planet.GetId() ).szName;
                if ( planet.GetStatus() == ePlanetStatus.unlocked )
                {
                    ui_planet.SetUnlock( true );
                    ui_main_land.SetUnlock(true);
                }
                else
                {
                    ui_planet.SetUnlock(false);
                    ui_main_land.SetUnlock(false);
                }
            } // end i > 0
        } // end for
    }

    public void UpdateUIDistrictsInfo()
    {
        Planet cur_show_detail_planet = GetPlanetById(m_nCurShowPlanetDetialIdOnUI);

        UIMainLand uiMainLand = m_aryMainLands[m_nCurShowPlanetDetialIdOnUI];
        UIBigMapTrackInfo[] aryUITrackInfo = uiMainLand.m_aryUITrakcs;

        District[] aryDistrict = cur_show_detail_planet.GetDistrictsList();
        for (int i = 0; i < aryDistrict.Length; i++)
        {
            UIBigMapTrackInfo ui_track = aryUITrackInfo[i];

            District district = aryDistrict[i];
            UIDistrict ui_district = m_aryUIDistricts[i];
            ui_district.SetDistrict(district);
            ui_track.SetTrack(district);

            ui_track._starsPrestige.SetStarLevel( district.GetPrestigeTimes() );

            if (i > 0) // 从第二个赛道开始
            {
                District district_prev = aryDistrict[i - 1];
                bool bPrevDistrictUnlock = (district_prev.GetStatus() == eDistrictStatus.unlocked);
                bool bThisDistrictUnlock = (district.GetStatus() == eDistrictStatus.unlocked);

                if (!bThisDistrictUnlock)
                {
                    ui_district.SetLocked(true, i);
                    ui_district.SetCanUnlock(bPrevDistrictUnlock);

                    ui_track.SetLocked(true, i);
                    ui_track.SetCanUnlock(bPrevDistrictUnlock);

                    ui_track._imgDataBg.gameObject.SetActive(bPrevDistrictUnlock);
                    ui_track._imgLock.gameObject.SetActive(bPrevDistrictUnlock);
                }
                else
                {
                    ui_district.SetLocked(false, i);
                    ui_district._txtUnlockPrice.gameObject.SetActive(false);
                    //    ui_district._goWenHao.SetActive(false);

                    ui_track.SetLocked(false, i);
                    ui_track._containerUnLockPrice.SetActive( false );

                    ui_track._imgDataBg.gameObject.SetActive(true);
                    ui_track._imgLock.gameObject.SetActive(false);

                }
                ui_district.m_bLocked = !bThisDistrictUnlock;
                ui_track.m_bLocked = !bThisDistrictUnlock;
            }

            bool bIsCurDistrict = MapManager.s_Instance.GetCurDistrict() == ui_track.GetTrack();//(i == GetCurDistrict().GetId() );
            ui_district._txtIsCurDistrict.gameObject.SetActive(bIsCurDistrict); ui_track._txtIsCurDistrict.gameObject.SetActive(bIsCurDistrict);
            ui_district._contaainerShouYi.gameObject.SetActive(!bIsCurDistrict); ui_district._contaainerShouYi.gameObject.SetActive(!bIsCurDistrict);
            if (!bIsCurDistrict) // 离线
            {
                float fTimeElaspe =(float)( Main.GetSystemTime() - district.GetStartOfflineTime() ).TotalSeconds;
                double fCurOfflineGain = fTimeElaspe * district.GetOfflineDps();
                ui_district._txtShouYi.text = fCurOfflineGain.ToString( "f0" ); ui_track._txtCurOfflineGain.text = "获利      " + fCurOfflineGain.ToString("f0");
                ui_track._txtAdsProfitLeftTime.text = fCurOfflineGain.ToString("f0"); ui_track._txtCurOfflineGain.text = "离线收益      " + CyberTreeMath.GetFormatMoney( fCurOfflineGain );

                ui_track._subcontainerProfitAndTime.SetActive(true);
            }
            else // 当前赛道
            {
                ui_track._subcontainerProfitAndTime.SetActive(false);
            }





            float fAdsLeftTime = district.GetAdsLeftTime();
            if (fAdsLeftTime > 0)
            {
                ui_district._txtTiSheng.text = "增益时间" + CyberTreeMath.FormatTime( (int)fAdsLeftTime );
                ui_track._txtAdsProfitLeftTime.text = "增益时间 " + CyberTreeMath.FormatTime((int)fAdsLeftTime);
            }
            else
            {
                ui_district._txtTiSheng.text = "未提升";
                ui_track._txtAdsProfitLeftTime.text = "未提升";
            }

            ui_district._imgMoneyIcon.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId(m_nCurShowPlanetDetialIdOnUI) ;//ResourceManager.s_Instance.m_aryCoinIcon[m_nCurShowPlanetDetialIdOnUI];
            ui_district._imgMoneyIcon_ShouYi.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId(m_nCurShowPlanetDetialIdOnUI);

            ui_district._imgMoneyIcon.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId(m_nCurShowPlanetDetialIdOnUI);//ResourceManager.s_Instance.m_aryCoinIcon[m_nCurShowPlanetDetialIdOnUI];
            ui_district._imgMoneyIcon_ShouYi.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId(m_nCurShowPlanetDetialIdOnUI);


        } // end for

    } // end 

    public string GetPlanetNameById(int nPlanetId)
    {
        DataManager.sPlanetConfig config = DataManager.s_Instance.GetPlanetConfigById(nPlanetId);
        return config.szName;
        switch(nPlanetId)
        {
            case 0:
                {
                    return "青铜星球";
                }
                break;
            case 1:
                {
                    return "白银星球";
                }
                break;
            case 2:
                {
                    return "黄金星球";
                }
                break;
        } // end switch

        return "";
    }

    public void OnClickButton_ClosePlanetDetailPanel()
    {
        _panelPanelDetails.SetActive( false );
        MapManager.s_Instance._subpanelBatCollectOffline.SetActive(false);
        m_bPlanetDetailPanelOpen = false;
    }

    District m_districtToUnlock = null;
    public void PreUnLockDistrict( District district )
    {
        m_districtToUnlock = district;
        _panelUnlockDistrict.SetActive( true );
        _txtUnlockDistrictCost.text = CyberTreeMath.GetFormatMoney(district.GetUnlockPrice()) ;//district.GetUnlockPrice().ToString();
        _imgUnlockTrackPriceIcon.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId( district.GetBoundPlanet().GetId() );
    }

    public void OnClick_CloseUnlockDistrictPanel()
    {
        _panelUnlockDistrict.SetActive( false );
    }

    public void OnClick_DoUnlockDistrict()
    {
        UnLockDistrict(m_nCurShowPlanetDetialIdOnUI, m_districtToUnlock.GetId());
    }

    Planet m_planetToUnlock = null;
    public void PreUnlockPlanet(int nPlanetId)
    {
        if (nPlanetId >= 2)
        {
            UIMsgBox.s_Instance.ShowMsg("本场景研发中，暂不开放.....");
            return;
        }

        Planet planet_prev = GetPlanetById(nPlanetId - 1);
        if ( planet_prev.GetStatus() != ePlanetStatus.unlocked )
        {
            UIMsgBox.s_Instance.ShowMsg( "请先解锁上一级场景" );
            return;
        }

        if (!planet_prev.CheckIfAllDistrictsUnlocked())
        {
            UIMsgBox.s_Instance.ShowMsg("请先把上一级场景所有赛道解锁");
            return;
        }

        m_planetToUnlock = GetPlanetById(nPlanetId);
        _panelUnlockPlanet.SetActive( true );
        _txtUnlockPlanetCost.text = CyberTreeMath.GetFormatMoney(m_planetToUnlock.GetUnlockCoinCost());//m_planetToUnlock.GetUnlockCoinCost().ToString();

        _imgUnlockPlanetPriceIcon.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId(m_planetToUnlock.GetId() - 1);

    }

    public void OnClick_CloseUnlockPlanetPanel()
    {
        _panelUnlockPlanet.SetActive( false );
    }

    public void OnClick_DoUnlockPlanet()
    {
        UnLockPlanet(m_planetToUnlock.GetId());
    }

    int m_nToEnterRace_PlanetId = 0;
    int m_nToEnterRace_DistrictId = 0;
    public void PreEnterRace( int nPlanetId, int nDistrictId )
    {
        m_nToEnterRace_PlanetId = nPlanetId;
        m_nToEnterRace_DistrictId = nDistrictId;

        m_fPreEnterRaceLoopTime = 1f;

        _panelEntering.SetActive( true );
    }

    float m_fPreEnterRaceLoopTime = 0;
    void PreEnterRaceLoop()
    {
        if (m_fPreEnterRaceLoopTime <= 0)
        {
            return;
        }
        m_fPreEnterRaceLoopTime -= Time.deltaTime;
        if (m_fPreEnterRaceLoopTime <= 0)
        {
            _panelEntering.SetActive(false);
            _panelPanelDetails.SetActive( false );
            Main.s_Instance.OnClickButton_CloseBigMap();
            EnterDistrict(m_nToEnterRace_PlanetId, m_nToEnterRace_DistrictId);
        }
    }

    public Planet[] GetPlanetList()
    {
        return m_aryPlanets;
    }

    bool m_bStarSkyMoving = false;
    public void StartStarSkyEffect()
    {
       // _goStarSky.SetActive( true );
        m_bStarSkyMoving = true;
    }

    public void StopStarSkyEffect()
    {
        _goStarSky.SetActive( false );
        m_bStarSkyMoving = false;

        Camera.main.transform.position = Vector3.zero;;
    }

    // 星空的移动 
    void StarSkyMoveLoop()
    {
        return;

        if (!m_bStarSkyMoving)
        {
            return;
        }

        vecTempPos = Camera.main.transform.position;
        vecTempPos.x -= m_fStarSkyMoveSpeed * Time.fixedDeltaTime;
        Camera.main.transform.position = vecTempPos;


        vecTempPos.x = -Input.gyro.attitude.y * 200f;
        vecTempPos.y = Input.gyro.attitude.x * 200f; ;
        vecTempPos.z = 0;
        _containerMainPlanets.transform.localPosition = vecTempPos;


        vecTempPos.x = -Input.gyro.attitude.y * 50f;
        vecTempPos.y = Input.gyro.attitude.x * 50f;
        vecTempPos.z = 0;
        _containerOtherPlanets.transform.localPosition = vecTempPos;

    }

    public void OnClick_CloseZengShouPanel()
    {
        _panelZengShou.SetActive( false );
    }


    public void OnClick_OpenZengShouPanel()
    {
        _panelZengShou.SetActive(true);

        foreach (KeyValuePair<string, UIZengShouCounter> pair in m_dicZengShouCounters)
        {
            pair.Value.transform.SetParent(_containerRecycledCounter.transform);
        }

        for (int i = 0; i < m_aryPlanetTitle.Length; i++ )
        {
            m_aryPlanetTitle[i].transform.SetParent(_containerRecycledCounter.transform);
        }

 
        int nPlanetId = -1;

        foreach ( KeyValuePair<string, UIZengShouCounter> pair in m_dicZengShouCounters)
        {
            if (nPlanetId != pair.Value.GetPlanetId() )
            {
                nPlanetId = pair.Value.GetPlanetId();
                m_aryPlanetTitle[nPlanetId].transform.SetParent(_containerZengShouCounters.transform); 
            }
          
           
            pair.Value.transform.SetParent( _containerZengShouCounters.transform );
            vecTempScale.x = 1f;
            vecTempScale.y = 1f;
            vecTempScale.z = 1f;
            pair.Value.transform.localScale = vecTempScale;
        }

        UpdateZengShouCountersOfflineDps();
    }

    public void SetZengShouCounterPrestigeTimes( int nPlanetId, int nDistrictId, int nPrestigeTimes )
    {
        UIZengShouCounter counter = m_dicZengShouCounters[nPlanetId + "_" + nDistrictId];
        counter._txtCurPrestigeTimes.text = nPrestigeTimes.ToString();

        int nPresitageRaise = DataManager.s_Instance.GetPrestigeGain(nPlanetId, nDistrictId, nPrestigeTimes);
        if (nPresitageRaise == 0)
        {
            nPresitageRaise = 1;
        }
        counter._txtPrestigeRaise.text = "x" + nPresitageRaise;
    }

    public void UpdateZengShouCountersOfflineDps()
    {
        foreach( KeyValuePair<string, UIZengShouCounter> pair in m_dicZengShouCounters )
        {
            UIZengShouCounter counter = pair.Value;
            int nPlanetId = counter.GetPlanetId();
            int nDistrictId = counter.GetDistrictId();
            Planet planet = GetPlanetById(nPlanetId);
            District district = planet.GetDistrictById(nDistrictId);
            district.CalculateOfflineDps();
            counter._txtOfflineDps.text = district.GetOfflineDps().ToString("f0") + "/秒";

        } // end foreach
    }

    public string GetDistrictNameByIndex( int nIndex )
    {
        return m_aryDistrictName[nIndex];
    }

    public void SetAllDiamondValueText( double nValue)
    {
        for (int i = 0; i < m_aryDiamondValue.Length; i++ )
        {
            Text txt = m_aryDiamondValue[i];
            if ( txt == null )
            {
                continue;
            }
            txt.text = CyberTreeMath.GetFormatMoney( nValue ) ;//nValue.ToString("f0");
        }
    }

    public void SetAllCoinValueText( int nPlanetId, double nValue )
    {
        Text[] aryCoinValue = null;
        if (nPlanetId == 0)
        {
            aryCoinValue = m_aryCoin0Value;
        }
        else if (nPlanetId == 1)
        {
            aryCoinValue = m_aryCoin1Value;
        }
        else if (nPlanetId == 2)
        {
            aryCoinValue = m_aryCoin2Value;
        }

        if (aryCoinValue == null)
        {
            return;
        }

        for (int i = 0; i < aryCoinValue.Length; i++ )
        {
            Text txt = aryCoinValue[i];
            if ( txt == null )
            {
                return;
            }

            txt.text = CyberTreeMath.GetFormatMoney(nValue  ) ;// nValue.ToString();
        }

    }

    public void CalculateAllPlanesSpeed()
    {
        m_CurDistrict.CalculateAllPlanesSpeed();
    }

    bool m_bTestShow = true;

    public void OnClick_Test()
    {
        _btnAdventure.gameObject.SetActive( !m_bTestShow);
    }

    bool m_bSliding = false;
    public bool m_bBigMapShowing = false;
    float m_fSlideSpeed = 0;
    float m_fSlideA = 0;
    float m_fV0 = 0;
    public GameObject _goSVContent;
    float m_fDestPos = 0;
    float m_fStartDragPos = 0;
    bool m_bDragging = false;
    float m_fMouseStartPos = 0;
    void BigMapLoop()
    {
        if (!m_bBigMapShowing)
        {
            return;
        }

        if ( Input.GetMouseButtonDown(0) )
        {
            if ( !m_bDragging)
            {
                m_fStartDragPos = _goSVContent.transform.localPosition.x;
                m_fMouseStartPos = Input.mousePosition.x;
            }
            m_bDragging = true;
        }

        if (Input.GetMouseButtonUp(0))
        {
            m_bClickProtected = false;

            m_bDragging = false;
            BeginSlide();

      
        }

        if (m_bDragging)
        {
            if ( Mathf.Abs(Input.mousePosition.x - m_fMouseStartPos) > 10 )
            {
               
                m_bClickProtected = true;
            }
        }


    }
    public bool m_bClickProtected = false;

    void Dragging()
    {
        if ( !m_bDragging )
        {
            return;
        }

        float fDesta = Input.mousePosition.x - m_fMouseStartPos;
        vecTempPos = _goSVContent.transform.localPosition;
        vecTempPos.x = m_fStartDragPos + fDesta;
        _goSVContent.transform.localPosition = vecTempPos;
    }
     
    void BeginSlide()
    {

        float fMinDis = 0;
        bool bFirst = true;

        float fRealDis = 0;
        int nDestIndex = 0;

        for (int i = 0; i < 3; i++)
        {
            float fDis = (m_aryTheMainlandCenters[i] - _goSVContent.transform.localPosition.x);
            float fAbsDis = Mathf.Abs( fDis );
            if (bFirst)
            {
                fMinDis = fAbsDis;
                bFirst = false;
                nDestIndex = i;
                continue;
            }
            if (fAbsDis < fMinDis)
            {
                fMinDis = fAbsDis;
                nDestIndex = i;
            }
        }
        fRealDis = m_aryTheMainlandCenters[nDestIndex] - _goSVContent.transform.localPosition.x;
        m_fV0 = fRealDis / m_fSlideTime;
        m_bSliding = true;
        m_fDestPos = m_aryTheMainlandCenters[nDestIndex];


        MapManager.s_Instance.OpenPlanetDetailPanel(nDestIndex);
        _txtCurPlanetName.text = GetPlanetNameById(nDestIndex);

    }

    void DoSliding()
    {
        if ( !m_bSliding)
        {
            return;
        }
        vecTempPos = _goSVContent.transform.localPosition;
        vecTempPos.x += m_fV0 * Time.fixedDeltaTime;
        _goSVContent.transform.localPosition = vecTempPos;
//        Debug.Log( "cur=" + _goSVContent.transform.localPosition.x);

        bool bEnd = false;
        if ( m_fV0 > 0 )
        {
            if ( vecTempPos.x >= m_fDestPos )
            {
                bEnd = true;
            }
        }else if (m_fV0 < 0)
        {
            if (vecTempPos.x <= m_fDestPos)
            {
                bEnd = true;
            }
        }
        else
        {
            bEnd = true;
        }
        if ( bEnd )
        {
            EndSlide();
        }
    }

    void EndSlide()
    {
        m_bSliding = false;

    }

    public UIMainLand[] m_aryMainLands;

} // end class
