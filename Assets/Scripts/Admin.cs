﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Admin : MonoBehaviour {

    static Vector3 vecTempScale = new Vector3();

    AdministratorManager.sAdminConfig m_Config ;

    int m_nColddownLeftTime = 0;
    int m_nDurationLeftTime = 0;

    bool m_bUsing = false;

    AdministratorManager.eUisngAdminStatus m_eStatus = AdministratorManager.eUisngAdminStatus.idle;

    System.DateTime m_dateSkillStartTime;
    float m_fColddownPercent = 0;

    UIAdministratorCounter m_BoundCounter = null;

    District m_BoundTrack = null;

    double m_nBuyPrice = 0; // 买入价 

    int[] m_aryIntParams = new int[8];


    // Use this for initialization
    void Start () {
       
    }
	
	// Update is called once per frame
	void Update () {

        SkillLoop();
      
	}

    public void Reset()
    {
        SetUsing( false );
        m_nColddownLeftTime = 0;
        m_nDurationLeftTime = 0;

        SetStatus(AdministratorManager.eUisngAdminStatus.idle);
    }

    public void SetBoundTrack( District track )
    {
        m_BoundTrack = track;
    }

    public void SetBoundCounter( UIAdministratorCounter counter )
    {
        m_BoundCounter = counter;
    }


    public UIAdministratorCounter GetBoundCounter()
    {
       return m_BoundCounter;
    }

    public void SetConfig( AdministratorManager.sAdminConfig config )
    {
        m_Config = config;
    }

    public void SetUsing( bool bUsing )
    {
        m_bUsing = bUsing;
    }

    public bool GetUsing()
    {
        return m_bUsing;
    }

    public AdministratorManager.sAdminConfig GetConfig()
    {
        return m_Config;
    }

    public void SetStatus( AdministratorManager.eUisngAdminStatus eStatus )
    {
        m_eStatus = eStatus;

    }

    public AdministratorManager.eUisngAdminStatus GetStatus()
    {
        return m_eStatus;
    }

    public void BeginCastSkill()
    {
        SetStatus(AdministratorManager.eUisngAdminStatus.casting);

        m_dateSkillStartTime = Main.GetSystemTime();
   
        m_nDurationLeftTime = m_Config.nDuration;

        switch ( m_Config.eType )
        {
            case AdministratorManager.eAdminFuncType.coin_raise:
                {
                    Main.s_Instance.UpdateRaise();
                }
                break;
            case AdministratorManager.eAdminFuncType.automobile_accelerate:
                {
                    Main.s_Instance.AccelerateAll( 1f + m_Config.fValue );
                }
                break;
            case AdministratorManager.eAdminFuncType.gain_immediately:
                {
                    ReCalculateDPSForGain();
                    // “立即获取一段时间的收益”改成持续技能了
                    /*
                    int nDps = (int)MapManager.s_Instance.GetCurDistrict().CalculateDPS();
                    int nGain = (int)( nDps * m_Config.fValue );
                    int nPlanetId = MapManager.s_Instance.GetCurPlanet().GetId();
                    int nCurCoin = AccountSystem.s_Instance.GetCoin(nPlanetId);
                    AccountSystem.s_Instance.SetCoin(nPlanetId, nCurCoin + nGain);
                    UIMsgBox.s_Instance.ShowMsg( nDps + " DPS x " + m_Config.fValue + " 秒 = " + nGain);
                    EndCastSkill(); // 瞬时技能，一发出就结束
                    */
                }
                break;
        } // end switch
    }

    float m_fTimeElaspe = 0;
    void SkillLoop()
    {
        m_fTimeElaspe += Time.deltaTime;
        if (m_fTimeElaspe < 1f) // 每秒轮询一次 
        {
            return;
        }
        m_fTimeElaspe = 0f;

        Casting_SkillLoop();
        ColdDown_SkillLoop();

    }

    void Casting_SkillLoop()
    {
        if ( GetStatus() != AdministratorManager.eUisngAdminStatus.casting )
        {
            return;
        }

        // 时间跨度有两种算法，依据游戏逻辑选择合适的算法
        // 1、如果要求必须在线计时的，就用走帧的方法计时
        // 2、如果可以离线计时的，就用当前时间减去起始时间
        int nTimeSpan = (int)((Main.GetSystemTime() - m_dateSkillStartTime).TotalSeconds );

        switch( m_Config.eType )
        {
            case AdministratorManager.eAdminFuncType.gain_immediately:
                {
                    SkillLoop_Gain();
                }
                break;
        } // end switch

        m_nDurationLeftTime = m_Config.nDuration - nTimeSpan;

        if (m_BoundTrack.IsCurTrack())
        {
            m_BoundCounter._progressbarColdDown.SetPercent((float)m_nDurationLeftTime/*nTimeSpan*/ / (float)m_Config.nDuration);
            m_BoundCounter._progressbarColdDown.SetTextContent("施放中:" + m_nDurationLeftTime);
        }
        if (m_nDurationLeftTime <= 0)
        {
            EndCastSkill();
        }

    }

    void ReCalculateDPSForGain()
    {
        int nDps = (int)MapManager.s_Instance.GetCurDistrict().CalculateDPS();
        int nTotalGain = (int)(nDps * m_Config.fValue);
        int nGainPerRound = nTotalGain / m_Config.nDuration; // 在持续时间内，每秒获得多少
        SetIntParams(0, nGainPerRound);
    }

    float m_fSkillGainTimeElaspe = 0f;
    void SkillLoop_Gain()
    {
        m_fSkillGainTimeElaspe += 1f;
     
        if (m_fSkillGainTimeElaspe < AdministratorManager.s_Instance.m_fSkillGainInterval)
        {
            return;
        }
        m_fSkillGainTimeElaspe = 0;
        
        ReCalculateDPSForGain();

        float nGainOfThisRound = GetIntParams(0);
        nGainOfThisRound *= AdministratorManager.s_Instance.m_fSkillGainInterval;
        AccountSystem.s_Instance.ChangeCoin(1, nGainOfThisRound);

      

        AudioManager.s_Instance.PlaySE(AudioManager.eSE.e_bank);

        vecTempScale.x = 1f;
        vecTempScale.y = 1f;
        vecTempScale.z = 1f;

        for (int i = 0; i < 30; i++)
        {
            UIFlyingCoin coin = ResourceManager.s_Instance.NewFlyingCoin();
            coin.transform.SetParent(UIManager.s_Instance._containerFlyingCoins.transform);

            coin.transform.localScale = vecTempScale;
            coin.transform.localPosition = AdministratorManager.s_Instance.m_vecCoinFlyStartPos;

            coin.m_vecSession0End.x = UnityEngine.Random.Range(AdministratorManager.s_Instance.m_vecCoinFlyMiddlePos_Left.x, AdministratorManager.s_Instance.m_vecCoinFlyMiddlePos_Right.x);
            coin.m_vecSession0End.y = UnityEngine.Random.Range(AdministratorManager.s_Instance.m_vecCoinFlyMiddlePos_Left.y, AdministratorManager.s_Instance.m_vecCoinFlyMiddlePos_Left.y);

            coin.m_vecEndPos = AdministratorManager.s_Instance.m_vecCoinFlyEndPos;

            float sX = coin.m_vecSession0End.x - coin.transform.localPosition.x;
            float sY = coin.m_vecSession0End.y - coin.transform.localPosition.y;
            float t = UIManager.s_Instance.m_fSession0Time;
            t = UnityEngine.Random.Range(t, t * 2);
            coin.m_vecSession0_A.x = 2f * sX / (t * t);
            coin.m_vecSession0_A.y = 2f * sY / (t * t);

            sX = AdministratorManager.s_Instance.m_vecCoinFlyEndPos.x - coin.m_vecSession0End.x;
            sY = AdministratorManager.s_Instance.m_vecCoinFlyEndPos.y - coin.m_vecSession0End.y;
            t = UIManager.s_Instance.m_fSession1Time;
            t = UnityEngine.Random.Range(t, t * 2);
            coin.m_vecSession1_A.x = 2f * sX / (t * t);
            coin.m_vecSession1_A.y = 2f * sY / (t * t);


            coin.BeginFly();
        } // end for
    }

    public int GetSkillCastingLeftTime()
    {
        return m_nDurationLeftTime;
    }

    public int GetSkillColdDownLeftTime()
    {
        return m_nColddownLeftTime;
    }

    void ColdDown_SkillLoop()
    {
        if (GetStatus() != AdministratorManager.eUisngAdminStatus.colddown)
        {
            return;
        }

        int nTimeSpan = (int)(Main.GetSystemTime() - m_dateSkillStartTime).TotalSeconds;
        m_nColddownLeftTime = m_Config.nColddown - nTimeSpan;
        int nColdDownLeftTime = m_Config.nColddown - nTimeSpan;
        m_fColddownPercent = (float)nColdDownLeftTime/ (float)m_Config.nColddown;

        if (m_BoundTrack.IsCurTrack())
        {
            m_BoundCounter._progressbarColdDown.SetPercent(m_fColddownPercent);
            m_BoundCounter._progressbarColdDown.SetTextContent("冷却中:" + nColdDownLeftTime);
        }
        if (nTimeSpan >= m_Config.nColddown)
        {
            EndColdDown();
        }

    }

    void EndColdDown()
    {
        SetStatus(AdministratorManager.eUisngAdminStatus.idle);

        AdministratorManager.s_Instance.EndColdDown();
    }

    public float GetColdDownPercent()
    {
        return m_fColddownPercent; 
    }

    public void EndCastSkill()
    {
        if ( GetStatus() != AdministratorManager.eUisngAdminStatus.casting ) // 不要重复执行
        {
            return;
        }

        SetStatus(AdministratorManager.eUisngAdminStatus.colddown);
        m_dateSkillStartTime = Main.GetSystemTime();

        switch( m_Config.eType )
        {
            case AdministratorManager.eAdminFuncType.coin_raise:
                {
                    Main.s_Instance.UpdateRaise();
                }
                break;
            case AdministratorManager.eAdminFuncType.automobile_accelerate:
                {
                    Main.s_Instance.StopAccelerateAll();
                }
                break;
        } // end switch


        AdministratorManager.s_Instance.EndCastSkill();
    }

    public void CancelCasting()
    {
        if (this.GetStatus() != AdministratorManager.eUisngAdminStatus.casting)
        {
            return;
        }

        this.EndCastSkill();
    }


    public void SetBuyPrice( double nValue )
    {
        m_nBuyPrice = nValue;
    }

    public double GetBuyPrice()
    {
        return m_nBuyPrice;
    }


    public void SetIntParams( int nIndex, int nValue )
    {
        m_aryIntParams[nIndex] = nValue;
    }

    public int GetIntParams(int nIndex )
    {
        return m_aryIntParams[nIndex];
    }

} // end class
