﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TanGeChe : MonoBehaviour {

    public static Vector3 vecTempScale = new Vector3();
    public static Vector3 vecTempPos = new Vector3();
    static Color colorTemp = new Color();

    public static TanGeChe s_Instance = null;

    public GameObject _goCounterList;
    List<UIVehicleCounter> m_lstVehicleCounters = new List<UIVehicleCounter>();

    public GameObject _panelTanGeChe;

    float m_fRealTimeDiscount = 0;


    public Sprite m_sprJianYingTemp;
    public Sprite m_sprLockedBuyButton;
    public Sprite m_spUnlockedBuyButton;

    public Color m_colorUnlocked;
    public Color m_colorLocked;


    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {


    }
	
	// Update is called once per frame
	void Update () {
        RandomEventLoop();

    }

    List<UIVehicleCounter> lstTempShowingCounters = new List<UIVehicleCounter>();
    List<UIVehicleCounter> lstTempShowingUnlockedCounters = new List<UIVehicleCounter>();

    public void LoadData( int nPlanetId, int nDistrictId )
    {
        lstTempShowingCounters.Clear();
        lstTempShowingUnlockedCounters.Clear();

        // 先清空车商城列表
        for (int iter = 0; iter < m_lstVehicleCounters.Count; iter++ )
        {
            m_lstVehicleCounters[iter].gameObject.SetActive( false );

            ResearchCounter rc = m_lstVehicleCounters[iter].GetBoundResearchCounter();
            if (rc)
            {
                rc.gameObject.SetActive(false);
            }
        }

        Dictionary<string, DataManager.sAutomobileConfig> dicAutomobileConfig = DataManager.s_Instance.GetAutomobileConfig();

        int i = 0;
        //for (int i = 0; i < ResourceManager.MAX_VEHICLE_NUM; i++ )
        foreach( KeyValuePair<string, DataManager.sAutomobileConfig> pair in dicAutomobileConfig  )
        {
            // 判断该交通工具是否适用于当前赛。如果不适用就跳过，不展示在工厂界面
            string szCurTrackId = MapManager.s_Instance.GetCurPlanet().GetId() + "_" + MapManager.s_Instance.GetCurDistrict().GetId();
            if ( pair.Value.szSuitableTrackId != szCurTrackId )
            {
                continue;
            }

            UIVehicleCounter counter = null;
            if (i < m_lstVehicleCounters.Count)
            {
                counter = m_lstVehicleCounters[i];
            }
            else
            {
                counter = ResourceManager.s_Instance.NewVehicleCounter();
                m_lstVehicleCounters.Add(counter);
            }

            counter.gameObject.SetActive( true );
            lstTempShowingCounters.Add( counter );

            int nLevel = pair.Value.nLevel; // 本交通工具的等级和“可解锁等级”之间并没有必然联系
            /*
            // 判断这个交通工具已解锁没有（当前赛道等级大于等于该交通工具解锁所需的等级，则自动解锁）
            if ( MapManager.s_Instance.GetCurDistrict().GetLevel() >= pair.Value.nCanUnlockLevel )
            {
                counter.SetUnlocked(true);
                lstTempShowingUnlockedCounters.Add(counter);

                // 还要判断该等级在“能源研究系统”中解锁没有
                ResearchCounter rc = MapManager.s_Instance.GetCurDistrict().GetResearchCounter();
                if (rc != null && rc.GetStatus() != ResearchManager.eResearchCounterStatus.unlocked )
                {
                    if (nLevel > rc.GetLevel())
                    {
                        counter.SetUnlocked(false);
                    }
                    else if (nLevel == rc.GetLevel())
                    {
                        //rc.transform.SetParent(counter._containerResearchLock.transform);
                        counter.SetBoundResearchCounter(rc);
                        if (rc.GetStatus() != ResearchManager.eResearchCounterStatus.unlocked)
                        {
                            rc.gameObject.SetActive(true);
                        }                       

                        vecTempScale.x = 1f;
                        vecTempScale.y = 1f;
                        vecTempScale.z = 1f;
                        rc.transform.localScale = vecTempScale;

                        vecTempPos.x = 0;
                        vecTempPos.y = 0;
                        vecTempPos.z = 0;
                        rc.transform.localPosition = vecTempPos;
                    }

                }
            }
            else
            {
                counter.SetUnlocked( false );
            }
            */

            int nBuyTimes = MapManager.s_Instance.GetCurDistrict().GetVehicleBuyTimeById( pair.Value.nId );
            double nInitialPrice = pair.Value.nStartPrice_CoinValue;//DataManager.s_Instance.GetVehiclePrice(nPlanetId, nDistrictId, nLevel);
            for (int j = 0; j < nBuyTimes; j++ )
            {
                nInitialPrice *= ( 1f + pair.Value.fPriceRaisePercentPerBuy );
            }
            double nRealPrice = nInitialPrice;

            // 主管的技能加成：可以降低售价
            float fAdminReduceCoinPricePercent = AdministratorManager.s_Instance.GetAutomobileCoinPriceReducePercent();
            nRealPrice *= (1f - fAdminReduceCoinPricePercent);

            /*
            // 主动技能加成(废弃)
            Skill skill = MapManager.s_Instance.GetCurDistrict().GetSkill(SkillManager.eSkillType.cost_reduce);
            float fSkillDiscount = 0;
            if (skill.GetStatus() == SkillManager.eSkillStatus.working)
            {
                fSkillDiscount = skill.m_Config.fValue;
                fRealDiscount += fSkillDiscount;
            }
            // end 主动技能加成
            */
            /*
            // 科技树加成(废弃)
            float fScienceDiscount = ScienceTree.s_Instance.GetVehicleBuyDiscount(nPlanetId);
            if (fScienceDiscount != 0)
            {
                fRealDiscount += fScienceDiscount;
            }
            */

            // end 科技树加成

            /*(废弃)
            if (fRealDiscount < -0.9f)
            {
                fRealDiscount = -0.9f;
            }
            nRealPrice = (1 + fRealDiscount) * nInitialPrice;

            m_fRealTimeDiscount = fRealDiscount;
            */

            double nGain = pair.Value.nBaseGain ; // DataManager.s_Instance.GetCoinGainperRound(nPlanetId, nDistrictId, nLevel);
            float fVehiclerunTimePerRound = pair.Value.fBaseSpeed ;// DataManager.s_Instance.GetVehicleRunTimePerRound(nLevel );

            counter._txtName.text = pair.Value.szName;
            counter._txtInitialPrice.text = "原价：" + nInitialPrice.ToString();
            counter._txtSkillDiscount.text = "技能折扣：" + (0.1f * 100 ) + "%";
         //   counter._txtScienceDiscount.text = "科技树折扣：" + (fScienceDiscount * 100) + "%"; ;

            counter.transform.SetParent( _goCounterList.transform );
            vecTempScale.x = 1f;
            vecTempScale.y = 1f;
            vecTempScale.z = 1f;
            counter.transform.localScale = vecTempScale;

            counter._txtLevel.text = "lv." + nLevel;

            int nSprIndex = nLevel - 1;
            if (nSprIndex >= ResourceManager.s_Instance.m_aryParkingPlaneSprites.Length)
            {
                Debug.LogError("nSprIndex >= ResourceManager.s_Instance.m_aryParkingPlaneSprites.Length");
                nSprIndex = 0;
            }

            counter._imgAvatar.sprite = ResourceManager.s_Instance.m_aryParkingPlaneSprites[nSprIndex];

            counter._txtPrice.text = CyberTreeMath.GetFormatMoney(nRealPrice) ;//nRealPrice.ToString("f0");

            // 根据金币种类不同，显示的金币图标不同
            counter._imgCoinType.sprite = ResourceManager.s_Instance.m_aryCoinIcon[(int)pair.Value.eCoinType];



            counter._txtGain.text = CyberTreeMath.GetFormatMoney( nGain ) ;// nGain.ToString();
            counter._txtSpeed.text = fVehiclerunTimePerRound.ToString();

            counter.m_nPrice = nRealPrice;
            counter.m_nDiamondPrice = pair.Value.nPriceDiamond;
            counter.m_nVehicleLevel = nLevel;

            counter._txtUnLockLevel.text = pair.Value.nCanUnlockLevel.ToString();

            counter.m_Config = pair.Value;

            counter.m_bWatchAdFree = false;





            // poppin test
            // 还要判断该等级在“能源研究系统”中解锁没有
            ResearchCounter rc = MapManager.s_Instance.GetCurDistrict().GetResearchCounter();
            int nTheLockLevel = -1;
            if (rc != null && rc.GetStatus() != ResearchManager.eResearchCounterStatus.unlocked)
            {
                nTheLockLevel = rc.GetLevel() + 1;
            }

            // 判断这个交通工具已解锁没有（当前赛道等级大于等于该交通工具解锁所需的等级，则自动解锁）
            if (MapManager.s_Instance.GetCurDistrict().GetLevel() >= pair.Value.nCanUnlockLevel) // 赛道达到了其可解锁等级
            {
                // counter.SetUnlocked(true);
                // lstTempShowingUnlockedCounters.Add(counter);
                if (nTheLockLevel <= 0)
                {
                    counter.SetUnlocked(true);
                }
                else
                {
                    if (nLevel > nTheLockLevel) // 其等级大于能源锁等级
                    {
                        counter.SetUnlocked(false);
                    }
                    else if (nLevel < nTheLockLevel )
                    {
                        counter.SetUnlocked(true);
                    }
                    else
                    {
                        counter.SetUnlocked(false);
                        counter.SetBoundResearchCounter(rc);
                        if (rc.GetStatus() != ResearchManager.eResearchCounterStatus.unlocked)
                        {
                            rc.gameObject.SetActive(true);
                        }

                        vecTempScale.x = 1f;
                        vecTempScale.y = 1f;
                        vecTempScale.z = 1f;
                        rc.transform.localScale = vecTempScale;

                        vecTempPos.x = 0;
                        vecTempPos.y = 0;
                        vecTempPos.z = 0;
                        rc.transform.localPosition = vecTempPos;
                    }


                 

                }
            }
 
            else// 赛道还没达到其可解锁等级
            {
                counter.SetUnlocked(false); 
            }


            /*/// right here
            // 判断这个交通工具已解锁没有（当前赛道等级大于等于该交通工具解锁所需的等级，则自动解锁）
            if (MapManager.s_Instance.GetCurDistrict().GetLevel() >= pair.Value.nCanUnlockLevel)
            {
                counter.SetUnlocked(true);
                lstTempShowingUnlockedCounters.Add(counter);

                // 还要判断该等级在“能源研究系统”中解锁没有
                ResearchCounter rc = MapManager.s_Instance.GetCurDistrict().GetResearchCounter();
                if (rc != null && rc.GetStatus() != ResearchManager.eResearchCounterStatus.unlocked)
                {
                    if (nLevel > rc.GetLevel())
                    {
                        counter.SetUnlocked(false);
                    }
                    else if (nLevel == rc.GetLevel())
                    {
                        //rc.transform.SetParent(counter._containerResearchLock.transform);
                        counter.SetBoundResearchCounter(rc);
                        if (rc.GetStatus() != ResearchManager.eResearchCounterStatus.unlocked)
                        {
                            rc.gameObject.SetActive(true);
                        }

                        vecTempScale.x = 1f;
                        vecTempScale.y = 1f;
                        vecTempScale.z = 1f;
                        rc.transform.localScale = vecTempScale;

                        vecTempPos.x = 0;
                        vecTempPos.y = 0;
                        vecTempPos.z = 0;
                        rc.transform.localPosition = vecTempPos;
                    }

                }
            }
            else
            {
                counter.SetUnlocked(false);
            }

            *///// right here


            /*
            if (i >= 4)
            {
                colorTemp = Color.black;
                colorTemp.a = 0.5f;
                counter._imgAvatar.color = colorTemp;
                counter._imgAvatar.sprite = m_sprJianYingTemp;
                counter._txtPrice.text = "???";
                counter._txtGain.text = "?";
                counter._txtSpeed.text = "?";
                counter._txtName.text = "???";
                counter._txtName.color = TanGeChe.s_Instance.m_colorUnlocked;
                counter._imgProgressBar_Gain.fillAmount = 0;
                counter._imgProgressBar_Speed.fillAmount = 0;

                counter._containerDiscount.SetActive( false );
                counter._containerGainAndSpeed.SetActive( false );
                counter._containerPrice.SetActive(false);

                counter._imgBtnBuy.sprite = TanGeChe.s_Instance.m_sprLockedBuyButton;

                counter._btnBuy.enabled = false;
            }
            else
            {

            }
            */

            i++;
        } // end foreach

        int nCount = 0;
        for (int iter = lstTempShowingCounters.Count - 1; iter >= 0; iter-- )
        {
            UIVehicleCounter counter = lstTempShowingCounters[iter];
            if (counter.GetUnlocked() && iter != 0)
            {
                //// 如果是已解锁的倒数第一或第二个，则显示用钻石购买 
                counter._imgCoinType.sprite = ResourceManager.s_Instance.m_aryCoinIcon[(int)DataManager.eMoneyType.diamond];
                counter._txtPrice.text = counter.m_nDiamondPrice.ToString();
                counter.m_eMoneyType = DataManager.eMoneyType.diamond;

                nCount++;
            }

            if ( nCount >= 2 )
            {
                break;
            }
        }


    }

    public float GetRealTimeDiscount()
    {
        return m_fRealTimeDiscount;
    }

    public void OnClick_OpenTanGeChe()
    {
        _panelTanGeChe.SetActive( true );

        //LoadData( MapManager.s_Instance.GetCurPlanet().GetId(), MapManager.s_Instance.GetCurDistrict().GetId());
        UpdateCarMallInfo();
    }

    public void OnClick_CloseTanGeChe()
    {
        _panelTanGeChe.SetActive(false);
    }

    public void UpdateCarMallInfo()
    {

        MapManager.s_Instance.GetCurPlanet().SetCoin(MapManager.s_Instance.GetCurPlanet().GetCoin());


      LoadData(MapManager.s_Instance.GetCurPlanet().GetId(), MapManager.s_Instance.GetCurDistrict().GetId());
    }

    ////////// 以下为“随机事件”模块
    public float m_fRandomEventInterval = 20f;
    float m_fRandomEventTimeElapse = 0f;
    bool m_bRandomEventProcessing = false;
    void RandomEventLoop()
    {
        m_fRandomEventTimeElapse += Time.deltaTime;
        if (m_fRandomEventTimeElapse >= m_fRandomEventInterval)
        {
            ExecRandomEvent();
        }
    }

    public void ExecRandomEvent()
    {
        if (m_bRandomEventProcessing)
        {
            return;
        }

        if (lstTempShowingUnlockedCounters.Count < 4)
        {
            return;
        }

        UIVehicleCounter counter = lstTempShowingUnlockedCounters[lstTempShowingUnlockedCounters.Count - 3];
        if ( counter.m_bWatchAdFree )
        {
            return;
        }

       
        counter.m_bWatchAdFree = true;
        counter._imgCoinType.sprite = ResourceManager.s_Instance.m_sprWatchAdIcon;
        counter._txtPrice.text = "免费";

        m_fRandomEventTimeElapse = 0f;
    }




} // end class
