﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIProgressBar : MonoBehaviour {

    public Image _imgBar;
    public Text _txtContent;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetPercent( float fPercent )
    {
        _imgBar.fillAmount = fPercent;
    }

    public void SetTextContent( string szContent )
    {
        _txtContent.text = szContent;
    }

} // end class
