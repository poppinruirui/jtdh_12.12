﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMsgBox : MonoBehaviour {

    public static UIMsgBox s_Instance = null;

    public GameObject _goContainerMain;
    public Text _txtMsg;

    float m_fTimeElapse = 0f;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Loop();
	}


    public void ShowMsg(string szMsg)
    {
        _txtMsg.text = szMsg;

        m_fTimeElapse = 1f;
        _goContainerMain.SetActive(true);
    }

    void Loop()
    {
        if ( m_fTimeElapse <= 0f )
        {
            return;
        }

        m_fTimeElapse -= Time.deltaTime;

        if (m_fTimeElapse <= 0f)
        {
            _goContainerMain.SetActive( false );
        }
    }

}
