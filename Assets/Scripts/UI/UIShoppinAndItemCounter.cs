﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIShoppinAndItemCounter : MonoBehaviour {

    /// <summary>
    /// UI
    /// </summary>
    public Button _btnBuy;
    public Button _btnUse;

    public GameObject _containerOneButton;
    public GameObject _containerTwoButton;
    public GameObject _containerItemUsingStatus;

    public Text _txtLeftTime;
    public Text _txtDuration;
    public Text _txtValue;

    public Text[] _aryTxtPriceDiamond;
    public Text _txtPriceCoin;

    public Image _imgAvatar;

    public Image _imgProgressBarFill;

    // end UI

    public float m_fValue = 0;
    public double m_dCoinPrice = 0;
    public int m_nDiamondPrice = 0;
    public int m_nDuration = 0;

    int m_nLeftTime = 0;

    public int m_nItemType = 0; // 0 - 用法币买钻石  1 - 收益强化道具  2 - 技能点
    public int m_nItemSubType = 0;
    public string m_szFuncDest = "";

    bool m_bItemInBag = false;

    public int m_nItemId = 0;

    System.DateTime m_dateStartTime;
    bool m_bWorking = false;

    // Use this for initialization
    void Start () {

        UpdateUIInfo();


    }
	
	// Update is called once per frame
	void Update () {
        Loop();

    }

    public void OnClick_LegalMoneyBuy_0()
    {
        AccountSystem.s_Instance.SetGreenCash(AccountSystem.s_Instance.GetGreenCash() + 300);
        AudioManager.s_Instance.PlaySE(AudioManager.eSE.e_buy_plane);
    }

    public void OnClick_LegalMoneyBuy_1()
    {
        AccountSystem.s_Instance.SetGreenCash(AccountSystem.s_Instance.GetGreenCash() + 400);
        AudioManager.s_Instance.PlaySE(AudioManager.eSE.e_buy_plane);
    }

    public void OnClick_LegalMoneyBuy_2()
    {
        AccountSystem.s_Instance.SetGreenCash(AccountSystem.s_Instance.GetGreenCash() + 300);
        AudioManager.s_Instance.PlaySE(AudioManager.eSE.e_buy_plane);
    }

    public void OnClick_CoinBuy()
    {
        if (m_nItemType != 2 )
        {
            return;
        }

        if ( AccountSystem.s_Instance.GetCoin( m_nItemSubType ) < m_dCoinPrice )
        {
            UIMsgBox.s_Instance.ShowMsg( "金币不足" );
            return;
        }

        double dCurCoin = AccountSystem.s_Instance.GetCoin(m_nItemSubType);
        dCurCoin -= m_dCoinPrice;
        AccountSystem.s_Instance.SetCoin(m_nItemSubType, dCurCoin);


        int nCurPoint = ScienceTree.s_Instance.GetSkillPoint((ScienceTree.eBranchType)m_nItemSubType);
        ScienceTree.s_Instance.SetSkillPoint((ScienceTree.eBranchType)m_nItemSubType, nCurPoint + 1);

        AudioManager.s_Instance.PlaySE(AudioManager.eSE.e_buy_plane);
    }

    public void OnClick_DiamondBuy()
    {
        int nCurDiamond = (int)AccountSystem.s_Instance.GetGreenCash();
        if (nCurDiamond < m_nDiamondPrice)
        {
            UIMsgBox.s_Instance.ShowMsg( "钻石不足 ");
            return;
        }

        ShoppinMall.s_Instance.ShowConfirmBuyPanel( this, 0 );

       
    }

    public void SetItemInBag( bool bItemInBag )
    {
        m_bItemInBag = bItemInBag;
        if (m_bItemInBag)
        {
            _btnBuy.gameObject.SetActive( false );
            _btnUse.gameObject.SetActive(true);
        }
    }

    public void OnClick_UseItem()
    {
        ItemSystem.s_Instance.UseItem( this );

    
    }

    public void CopyData( UIShoppinAndItemCounter src_counter  )
    {
        m_nDuration = src_counter.m_nDuration;
        m_nItemType = src_counter.m_nItemType;
        m_fValue = src_counter.m_fValue;
        m_nItemId = src_counter.m_nItemId;
        UpdateUIInfo();
    }

    void UpdateUIInfo()
    {
        if ( m_nItemType == 0 )
        {
            return;
        
        }

        _txtPriceCoin.text = m_dCoinPrice.ToString("f0");
        for (int i = 0; i < 2; i++)
        {
            _aryTxtPriceDiamond[i].text = m_nDiamondPrice.ToString();
        }

        if (m_nItemType == 2)
        {
            return;
        }
        _txtValue.text = "x" + (1f + m_fValue) + "倍";
        _txtDuration.text = CyberTreeMath.FormatTime(m_nDuration, 1);
 



    }

    public void Begin()
    {
        m_dateStartTime = Main.GetSystemTime();
        m_bWorking = true;
        m_nLeftTime = m_nDuration;


        _containerItemUsingStatus.SetActive( true );
        _containerOneButton.SetActive( false );
        _containerTwoButton.SetActive(false);
    }

    float m_fTimeElapse = 0;
    void Loop()
    {
        if ( !m_bWorking)
        {
            return;
        }

        m_fTimeElapse += Time.deltaTime;
        if (m_fTimeElapse < 1f)
        {
            return;
        }
        m_fTimeElapse = 0;

        double fElapse = (Main.GetSystemTime() - m_dateStartTime).TotalSeconds;

        int fTimeLeft = m_nDuration - (int)fElapse;
        _imgProgressBarFill.fillAmount = (float)fElapse / (float)m_nDuration;
        _txtLeftTime.text = CyberTreeMath.FormatTime(fTimeLeft);
        if (fTimeLeft <= 0)
        {
            End();
        }


    }

    public void AddTime( int fTime )
    {
        m_nLeftTime += fTime;
        m_nDuration += fTime;
    }

    public void End()
    {
        ResourceManager.s_Instance.DeleteShoppinAndItemCounter( this );
    }

} // end class
