﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBigMapTrackInfo : MonoBehaviour {

    /// <summary>
    /// UI
    /// </summary>
    public GameObject _containerUnLockPrice;
    public GameObject _containerProfitAndTime;
    public GameObject _txtIsCurDistrict;
    public GameObject _subcontainerProfitAndTime;

    public UIStars _starsPrestige;
    public Text _txtTrackName;
    public Text _txtCurOfflineGain;
    public Text _txtAdsProfitLeftTime;
    public Button _imgLock;
    public Text _txtUnlockPrice;
    public Image _imgUnlockCoinType;
    public Image _imgProfitCoinType;
    public Image _imgDataBg;

    public Button _btnEnter;
    // end ui

    District m_District = null;


    public MapManager.eDistrictStatus m_eStatus = MapManager.eDistrictStatus.unlocked;
    bool m_bCanUnlock = false;
    public bool m_bLocked = true;

    float m_fBtnEnterShowTime = 0f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
        if (m_fBtnEnterShowTime <= 0)
        {
            return;
        }
        m_fBtnEnterShowTime -= Time.deltaTime;
        if (m_fBtnEnterShowTime <= 0f)
        {
            _btnEnter.gameObject.SetActive(false);
        }


    }

    public void SetTrack( District district )
    {
        m_District = district;

        DataManager.sTrackConfig config = DataManager.s_Instance.GetTrackConfigById(district.GetBoundPlanet().GetId(), district.GetId());
        SetUnlockPrice(config.nUnlockPrice  /*district.GetUnlockPrice()*/ );
        _txtTrackName.text = config.szName;
    }

    public District GetTrack()
    {
        return m_District;
    }

    public void SetUnlockPrice(double nPrice)
    {
        _txtUnlockPrice.text = "解锁      " + CyberTreeMath.GetFormatMoney(nPrice);//nPrice.ToString("f0");
    }

    public void SetStatus(MapManager.eDistrictStatus eStatus)
    {
        m_eStatus = eStatus;
    }

    public MapManager.eDistrictStatus GetStatus()
    {
        return m_eStatus;
    }

    public void SetLocked(bool bLocked, int nCounterIndex)
    {
        _containerUnLockPrice.SetActive(bLocked);
        _containerProfitAndTime.SetActive( !bLocked);
       
    }

    public void SetCanUnlock(bool bCan)
    {
        m_bCanUnlock = bCan;

        _containerUnLockPrice.SetActive(m_bCanUnlock);
    }

    public void OnClickMe()
    {
        if ( MapManager.s_Instance.m_bClickProtected )
        {
            return;
        }

        if (m_bLocked)
        {
            if (m_bCanUnlock)
            {
                MapManager.s_Instance.PreUnLockDistrict(m_District);
            }
            else
            {
                UIMsgBox.s_Instance.ShowMsg("请先解锁上一级赛道");
            }
        }
        else
        {

            // 直接进入该赛道
            //MapManager.s_Instance.PreEnterRace(MapManager.s_Instance.m_nCurShowPlanetDetialIdOnUI, m_District.GetId());
            _btnEnter.gameObject.SetActive( true );
            m_fBtnEnterShowTime = 2f;

           
        }
    }

    public void OnClick_Enter()
    {
        MapManager.s_Instance.PreEnterRace(MapManager.s_Instance.m_nCurShowPlanetDetialIdOnUI, m_District.GetId());
        UIMsgBox.s_Instance.ShowMsg("正在进入，请稍候...");
    }



} // end class

