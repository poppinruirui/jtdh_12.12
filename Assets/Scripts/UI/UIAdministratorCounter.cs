﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIAdministratorCounter : MonoBehaviour {

    /// <summary>
    /// UI
    /// </summary>
    public Text _txtName;            // 名称
    public Text _txtQuality;         // 品质
    public Text _txtDuration;        // 持续时间
    public Text _txtDesc;            // 功能描述
    public Button _btnUseOrCancel;   //  "指派"或"取消指派"
    public Button _btnSell;          // 卖出
    public Button _btnCancel;

    public Image _imgAvatar;         // 主管头像
    public Image _imgBg;
    public Image _imgSkillIcon;



    public Text _txtOperateTitle;    // 操作

    public UIProgressBar _progressbarColdDown; // ColdDown进度条
    // end UI

    AdministratorManager.sAdminConfig m_Config;

    Admin m_BoundAdmin = null;

    bool m_bUsing = false;

	// Use this for initialization
	void Start () {
        Reset();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetConfig(AdministratorManager.sAdminConfig config )
    {
        m_Config = config;

        _txtName.text = m_Config.szName;
        _txtDesc.text = m_Config.szDesc;
        _txtQuality.text = m_Config.szQuality;
        _txtDuration.text = "持续:" + m_Config.nDuration + "秒";
        _imgSkillIcon.sprite = AdministratorManager.s_Instance.m_arySkillIcons[(int)m_Config.eType];
    }

    public void Reset()
    {
        SetOperateTitle("指派");
        SetUsing(false );

        _progressbarColdDown.gameObject.SetActive( false );
    }

    public void SetOperateTitle( string szTitle )
    {
        _txtOperateTitle.text = szTitle;
    }

    public void SetUsing( bool bUsing )
    {
        m_bUsing = bUsing;

        _btnSell.gameObject.SetActive(!m_bUsing);
        if ( m_bUsing )
        {
            //_txtOperateTitle.text = "取消";
            _btnSell.gameObject.SetActive( false );
            _btnUseOrCancel.gameObject.SetActive(false);
            _btnCancel.gameObject.SetActive(true);
            _imgBg.sprite = AdministratorManager.s_Instance.m_sprUsingBg;
            _txtName.color = AdministratorManager.s_Instance.m_colorUsing;
        }
        else
        {
            //_txtOperateTitle.text = "指派";
            _btnSell.gameObject.SetActive(true);
            _btnUseOrCancel.gameObject.SetActive(true);
            _btnCancel.gameObject.SetActive(false);
            _imgBg.sprite = AdministratorManager.s_Instance.m_sprNotUsingBg;
            _txtName.color = AdministratorManager.s_Instance.m_colorNotUsing;
        }
    }

    public void OnClickButton_UseOrCancel()
    {
        if ( m_bUsing )
        {
            DoCancel();
        }
        else
        {
            DoUse();
        }
    }

    public void OnClickButton_Cancel()
    {
        DoCancel();
    }


    public void OnClickButton_Sell()
    {
        AdministratorManager.s_Instance.SellAdmin( GetBoundAdmin() );       
    }

    void DoUse()
    {
        AdministratorManager.s_Instance.CancelCastingSkill();


        AdministratorManager.s_Instance.DoUse( this );
    }

    void DoCancel()
    {
        AdministratorManager.s_Instance.DoCancel( this );
    }

    public void Bind( Admin admin )
    {
        m_BoundAdmin = admin;
        SetConfig( admin.GetConfig() );

        admin.SetBoundCounter( this );

        if ( admin.GetStatus() != AdministratorManager.eUisngAdminStatus.idle )
        {
            _progressbarColdDown.gameObject.SetActive( true );
        }
    }

    public Admin GetBoundAdmin()
    {
        return m_BoundAdmin;
    }

    public Sprite GetAvatar()
    {
        return _imgAvatar.sprite;
    }

    // to do 
    // 1、取消指派时，如果正在施放技能，则技能取消
    // 2、取消指派时，Colddown不清零。




} // end class

