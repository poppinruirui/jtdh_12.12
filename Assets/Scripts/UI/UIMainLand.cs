﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMainLand : MonoBehaviour {

    public UIBigMapTrackInfo[] m_aryUITrakcs;
    public GameObject _containerTrakcs;

    public Image _imgMain;

    public GameObject _containerUnlockPrice;
    public Text _txtUnlockPrice;
    public Image _imgUnlockIcon;


    public bool m_bLocked = true;

    public int m_nPlanetId = 0;

    public Button _btnClickMe;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClickMe()
    {
        if (m_bLocked)
        {
            //MapManager.s_Instance.UnLockPlanet(m_nPlanetId);
            MapManager.s_Instance.PreUnlockPlanet(m_nPlanetId);
        }
    }

    public void SetUnlockPrice(double nPrice, int nPlanetId)
    {
        _imgUnlockIcon.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId(nPlanetId);
        _txtUnlockPrice.text = "解锁：" + CyberTreeMath.GetFormatMoney(nPrice); // nPrice.ToString( "f0" );
    }

    public void SetUnlock(bool bUnlock)
    {

        _containerUnlockPrice.SetActive(!bUnlock);

        m_bLocked = !bUnlock;

        if (bUnlock)
        {
            _imgMain.color = Color.white;

        }
        else
        {
            _imgMain.color = Color.black;
        }

        _containerTrakcs.SetActive(bUnlock);
        _btnClickMe.gameObject.SetActive(!bUnlock);
    }





} // end class
