﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JTDHSceneManager : MonoBehaviour {

    public static JTDHSceneManager s_Instance = null;
    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    int m_nSceneId = 0;

    /// <summary>
    /// /资源
    /// </summary>

    //// 各种prefab
    public GameObject m_preSnowFlake; // 雪花

    // 每个场景的主地图图片
    public Sprite[] m_aryMainBg;

    // 泊位格子图片
    public Sprite[] m_aryLot;

    // 场景融合遮罩色 
    public Color[] m_aryLotMaskColor;

    // 各种场景附件的容器
    public GameObject[] m_arySceneAccessoriesContainers;

    public GameObject m_containerSnowFlakes;

    /// <summary>
    /// 
    /// </summary>
    public SpriteRenderer _srMainBg;

    // 雪花相关
    public float m_fSnowMoveSpeed; // 雪花的下落速度
    public float m_fSnowMoveStartPosLeft; // 雪花出现的最左边位置
    public float m_fSnowMoveStartPosRight; // 雪花出现的最右边位置
    public float m_fSnowMoveEndPosHigh; // 雪花下落的终点高位
    public float m_fSnowMoveEndPosLow; // 雪花下落的终点低位
    public float m_fSnowGenerateInterval; // 雪花生成的时间间隔
    public float m_fSnowStartPosY;
    public float m_fSnowBeginFadePosY; // 开始渐隐的高度



    float m_fSnowStartPosX = 0f;
    float m_fSnowEndPosY = 0f;
    float m_fSnowDropMovementPerFrame = 0f;

    // 沙尘相关
    public float m_fSandStormMoveSpeed; // 沙尘移动的速度
    public float m_fSandStormMoveStartPos; // 沙尘移动的起点
    public float m_fSandStormMoveEndPos; // 沙尘移动的终点
    public SpriteRenderer[] m_arySandStorm; // 沙尘的图片

    float m_fSandStormMovementPerFrame = 0f;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {

        /// 沙尘相关的一些数值计算、初始化
        m_fSandStormMovementPerFrame = m_fSandStormMoveSpeed * Time.fixedDeltaTime;

        // 雪花相关的一些数值计算、初始化
        m_fSnowDropMovementPerFrame = m_fSnowMoveSpeed * Time.fixedDeltaTime;

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void FixedUpdate()
    {
        Scenario0_Loop();
        Scenario1_Loop();
        Scenario2_Loop();
    }


    public void ChangeScene( int nSceneId )
    {
        m_nSceneId = nSceneId;

        _srMainBg.sprite = m_aryMainBg[nSceneId]; // 更换场景底图

        // 当前场景的场景附件显示，其余场景的场景附件隐藏
        for (int i = 0; i < m_arySceneAccessoriesContainers.Length; i++ )
        {
            GameObject goAccessoryContainer = m_arySceneAccessoriesContainers[i];
            if (goAccessoryContainer == null)
            {
                continue;
            }
            goAccessoryContainer.SetActive(m_nSceneId == i);
        }

        // 遍历所有泊位格子，更新其图片
        for (int i = 0; i < Main.s_Instance.m_aryLots.Length; i++ )
        {
            Lot lot = Main.s_Instance.m_aryLots[i];
            lot.SetMainSpr( m_aryLot[m_nSceneId] );
            lot._enviromentMask._srEnviromnet.color = GetLotBlendEnviromentColor(m_nSceneId);
        }

    }

    public Color GetLotBlendEnviromentColor( int nPlanetId )
    {
        return m_aryLotMaskColor[nPlanetId];
    }

    void SandStormMove()
    {
        for (int i = 0; i < m_arySandStorm.Length; i++ )
        {
            SpriteRenderer srSand = m_arySandStorm[i];
            vecTempPos = srSand.transform.localPosition;
            vecTempPos.x += m_fSandStormMovementPerFrame;

            if ( vecTempPos.x < m_fSandStormMoveEndPos )
            {
                vecTempPos.x = m_fSandStormMoveStartPos;
            }

            srSand.transform.localPosition = vecTempPos;

           

        }
    }


    void Scenario0_Loop()
    {
        if ( m_nSceneId != 0 )
        {
            return;
        }

        SnowGenerateLoop();
        SnowDropLoop();
    }

    void Scenario1_Loop()
    {
        if (m_nSceneId != 1)
        {
            return;
        }

        SandStormMove();
    }

    void Scenario2_Loop()
    {
        if (m_nSceneId != 2)
        {
            return;
        }
    }


    float m_fSnowGenerateTimeElapse = 0;
    List<SnowFlake> m_lstSnowFlakes = new List<SnowFlake>();
    void SnowGenerateLoop()
    {
        m_fSnowGenerateTimeElapse += Time.fixedDeltaTime;
        if (m_fSnowGenerateTimeElapse < m_fSnowGenerateInterval)
        {
            return;
        }
        m_fSnowGenerateTimeElapse = 0;

        SnowFlake snow = NewSnowFlake();
        vecTempPos.x = UnityEngine.Random.RandomRange( m_fSnowMoveStartPosLeft, m_fSnowMoveStartPosRight );
        vecTempPos.y = m_fSnowStartPosY;
        vecTempPos.z = 0;
        snow.transform.localPosition = vecTempPos;
        snow.transform.SetParent( m_containerSnowFlakes.transform );
        m_lstSnowFlakes.Add(snow);
        float fEndPosY = UnityEngine.Random.RandomRange(m_fSnowMoveEndPosLow, m_fSnowMoveEndPosHigh);
        snow.SetStartPos(vecTempPos.x, m_fSnowStartPosY);
        snow.SetDropEndPosY(fEndPosY);
        snow.SetScale(UnityEngine.Random.RandomRange(0.8f, 1.0f));
        float fDropDis = m_fSnowBeginFadePosY - fEndPosY;
        float fDropTime = fDropDis / m_fSnowMoveSpeed;
        float fFadeSpeed = 1f / fDropTime;
        float fFadePerFrame = fFadeSpeed * Time.fixedDeltaTime;
        snow.SetFadeParams(fFadePerFrame, m_fSnowBeginFadePosY);
    }

    void SnowDropLoop()
    {
        for (int i = m_lstSnowFlakes.Count - 1; i >= 0; i-- )
        {
            SnowFlake snow = m_lstSnowFlakes[i];
            if ( snow.Drop(m_fSnowDropMovementPerFrame, m_fSnowEndPosY) )
            {
                m_lstSnowFlakes.Remove( snow );
                DeleteSnowFlake(snow);
            }
        }
    }

    public SnowFlake NewSnowFlake()
    {
        return GameObject.Instantiate( m_preSnowFlake ).GetComponent<SnowFlake>();
    }

    public void DeleteSnowFlake(SnowFlake snow )
    {
        GameObject.Destroy( snow.gameObject );
    }

} // end class
