﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillManager : MonoBehaviour
{

    public static SkillManager s_Instance = null;

    public GameObject _containerAcclerateLights;

    public enum eSkillType
    {
        speed_accelerate, // 增加绕圈速度
        coin_raise,       // 增加金币收益
        cost_reduce,      // 减少金钱花费

        total_num,
    };

    public struct sSkillConfig
    {
        public float fValue;    // 核心属性值
        public float nColdDown; // ColdDown时间
        public float nDuration; // 持续时间

    };

    public enum eSkillStatus
    {
        idle,
        working,
        colddown,
    };

    public const int MAX_SKILL_LEVEL = 20;

    Dictionary<eSkillType, sSkillConfig[]> m_dicSkillConfig = new Dictionary<eSkillType, sSkillConfig[]>();

    /// <summary>
    /// UI
    /// </summary>
    public UISkillCastCounter[] m_arySkillButton;

    // UI

    private void Awake()
    {
        s_Instance = this;
    }
    sSkillConfig tempConfig;
    // Use this for initialization
    void Start()
    {
        InitSkillConfig();


       
       // GetSkillConfig(eSkillType.speed_accelerate, 1, ref tempConfig);
       // m_arySkillButton[0].SetValue(tempConfig.fValue);


    }

    // Update is called once per frame
    void Update()
    {

    }

   

    public void InitSkillConfig()
    {
        sSkillConfig[] arySpeedAccelerate = new sSkillConfig[MAX_SKILL_LEVEL];
        sSkillConfig[] aryCoinRaise = new sSkillConfig[MAX_SKILL_LEVEL];
        sSkillConfig[] aryCostReduce = new sSkillConfig[MAX_SKILL_LEVEL];
        for (int i = 0; i < 20; i++)
        {
            int nLevel = i + 1;

            sSkillConfig config = new sSkillConfig();
            config.fValue = 1f + nLevel;
            config.nColdDown = 10;
            config.nDuration = 10;
            arySpeedAccelerate[i] = config;

            config = new sSkillConfig();
            config.fValue = 1 + nLevel;
            config.nColdDown = 10;
            config.nDuration = 10;
            aryCoinRaise[i] = config;

            config = new sSkillConfig();
            config.fValue = 0.05f * nLevel;
            if (config.fValue > 0.8f)
            {
                config.fValue = 0.8f;
            }
            aryCostReduce[i] = config;

            m_dicSkillConfig[eSkillType.speed_accelerate] = arySpeedAccelerate;
            m_dicSkillConfig[eSkillType.coin_raise] = aryCoinRaise;
            m_dicSkillConfig[eSkillType.cost_reduce] = aryCostReduce;

        } // end i
    }

    public void GetSkillConfig(eSkillType eType, int nLevel, ref sSkillConfig tempConfig)
    {
        sSkillConfig[] ary = null;
        if (!m_dicSkillConfig.TryGetValue(eType, out ary))
        {
            Debug.LogError(" bug!!!!!");
            return;
        }
        int nIndex = nLevel - 1;
        if (nIndex < 0 || nIndex >= ary.Length)
        {
            Debug.LogError(" bug!!!!!");
            return;
        }

        tempConfig = ary[nIndex];
    }

    public void UpdateSkillButtonsStatus()
    {
        /*
        District cur_district = MapManager.s_Instance.GetCurDistrict();
        for (int i = 0; i < m_arySkillButton.Length; i++ )
        {
            SceneUiSkillButton button = m_arySkillButton[i];
            Skill skill = cur_district.GetSkill((eSkillType)i);
            switch( skill.GetStatus() )
            {
                case eSkillStatus.colddown:
                    {
                        button._txtStatus.text = "ColdDown...";

                    }
                    break;
                case eSkillStatus.idle:
                    {
                        button._txtStatus.text = "(可用)";
                    }
                    break;
                case eSkillStatus.working:
                    {
                        button._txtStatus.text = "使用中";
                    }
                    break;
            } // end switch

            switch( skill.GetSkillType() )
            {
                case  eSkillType.speed_accelerate:
                    {
                        button._txtCurValue.text = "X" + skill.m_Config.fValue;
                    }
                    break;
                case eSkillType.coin_raise:
                    {
                        button._txtCurValue.text = "X" + skill.m_Config.fValue;
                    }
                    break;
                case eSkillType.cost_reduce:
                    {
                        button._txtCurValue.text = "-" + (skill.m_Config.fValue * 100 ) + "%";
                    }
                    break;
            } // end switch

            if ( skill.GetStatus() == eSkillStatus.idle )
            {
                button._txtLeftTime.text = "";
            }
            else
            {
                float fTimeElapse =( Main.GetSystemTime() - skill.GetStartTime()).TotalSeconds;
                float fTimeLeft = skill.m_Config.nDuration - (int)fTimeElapse;
                button._txtLeftTime.text = fTimeLeft.ToString();
            }

        } // end for
        */
    }



} // end class

