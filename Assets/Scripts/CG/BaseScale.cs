﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseScale : MonoBehaviour {

     Vector3 vecTempScale = new Vector3();

    public bool m_bAutoaPlay = false;
    public bool m_bLoop = false;
    public bool _bHideWhenEnd = false;

    public float m_fScaleTime = 0.2f;
    public float m_fWaitTime = 0.2f;
    public float m_fMaxScale = 1.5f;

    int m_nStatus = 0; // 0 - none  1 - Change  2 - wait  3 - back  

    float m_fSpeed = 0f;
    float m_fRotateSpeed = 0f;
    float m_fWaitTimeElapse = 0f;

    public float m_fInitScale = 0f;
    float m_fDestScale = 0f;

    public bool _bWithRotate = false;
    public float _fMaxRotateAngle = 0f;
    float m_fAngle = 0f;

    public GameObject _goShadow;
    bool m_bShadowBegin = false;

    public enum eScaleAxis
    {
        all, 
        x,
        y
    };

    public eScaleAxis m_eAxis = eScaleAxis.all;

    // Use this for initialization
    void Start () {
		
        if (m_bAutoaPlay)
        {
            BeginScale();
        }


	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void FixedUpdate()
    {
        Scaling();
    }

    public void BeginScale()
    {


        this.gameObject.SetActive( true );

        m_bShadowBegin = false;
        m_nStatus = 1;
        m_fWaitTimeElapse = 0f;

   
        m_fDestScale = m_fInitScale * m_fMaxScale;
        m_fSpeed = ( m_fDestScale - m_fInitScale ) / m_fScaleTime * Time.fixedDeltaTime;

        vecTempScale.x = m_fInitScale;
        vecTempScale.y = m_fInitScale;
        vecTempScale.z = 1f;
        this.transform.localScale = vecTempScale;

        m_fRotateSpeed = _fMaxRotateAngle / m_fScaleTime * Time.fixedDeltaTime;

        m_fAngle = 0f;
    }

    void Scaling()
    {

        if (m_nStatus == 0)
        {
            return;
        }

        if (m_nStatus == 1)
        {
            vecTempScale = this.transform.localScale;
            float fScale = 1f;
            if ( m_eAxis == eScaleAxis.x )
            {
                fScale = vecTempScale.x;
            }
            else if (m_eAxis == eScaleAxis.y)
            {
                fScale = vecTempScale.y;
            }
            else
            {
                fScale = vecTempScale.x;
            }

            fScale += m_fSpeed;
            if (m_eAxis == eScaleAxis.x)
            {
                vecTempScale.x = fScale;
            }
            else if (m_eAxis == eScaleAxis.y)
            {
                vecTempScale.y = fScale;
            }
            else
            {
                vecTempScale.x = fScale;
                vecTempScale.y = fScale;
            }

            vecTempScale.z = 1f;
            this.transform.localScale = vecTempScale;

            m_fAngle += m_fRotateSpeed;
            this.transform.localRotation = Quaternion.identity;
            this.transform.Rotate(0.0f, 0.0f, m_fAngle);

            if (m_fSpeed > 0)
            {
                if (fScale >= m_fDestScale)
                {
                    m_nStatus = 2;
                }
            }
            else if (m_fSpeed < 0)
            {
                if (fScale <= m_fDestScale)
                {
                    m_nStatus = 2;
                }
            }
        }
        else if (m_nStatus == 2)
        {
            m_fWaitTimeElapse += Time.fixedDeltaTime;
            if (m_fWaitTimeElapse >= m_fWaitTime)
            {
                m_nStatus = 3;

                /*
                if (_goShadow)
                {
                    _goShadow.SetActive(true);
                }
                */
            }
        }
        else if (m_nStatus == 3)
        {
            /*
            vecTempScale = this.transform.localScale;
            vecTempScale.x -= m_fSpeed;
            vecTempScale.y -= m_fSpeed;
            vecTempScale.z = 1f;
            */
            float fScale = 1f;
            if (m_eAxis == eScaleAxis.x)
            {
                fScale = vecTempScale.x;
            }
            else if (m_eAxis == eScaleAxis.y)
            {
                fScale = vecTempScale.y;
            }
            else
            {
                fScale = vecTempScale.x;
            }

            fScale -= m_fSpeed;
            if (m_eAxis == eScaleAxis.x)
            {
                vecTempScale.x = fScale;
            }
            else if (m_eAxis == eScaleAxis.y)
            {
                vecTempScale.y = fScale;
            }
            else
            {
                vecTempScale.x = fScale;
                vecTempScale.y = fScale;
            }

            vecTempScale.z = 1f;
            this.transform.localScale = vecTempScale;


            m_fAngle -= m_fRotateSpeed;
            this.transform.localRotation = Quaternion.identity;
            this.transform.Rotate(0.0f, 0.0f, m_fAngle);

            bool bEnd = false;
            if ( m_fSpeed > 0 )
            {
                if (fScale <= m_fInitScale )
                {
                    bEnd = true;
                }
            }
            else if (m_fSpeed < 0)
            {
                if (fScale >= m_fInitScale)
                {
                    bEnd = true;
                }
            }
                
            if (bEnd/*vecTempScale.x <= m_fInitScale */ )
            {
                vecTempScale.x = m_fInitScale;
                vecTempScale.y = m_fInitScale;
                m_nStatus = 0;

                if ( _bHideWhenEnd )
                {
                    this.gameObject.SetActive( false );
                }

                if (_goShadow)
                {
                    _goShadow.SetActive(false);
                }

                if (m_bLoop)
                {
                    BeginScale();
                }
            }

            this.transform.localScale = vecTempScale;


            /*
            if (_goShadow)
            {
                if (m_bShadowBegin)
                {
                    vecTempScale.x *= 1.3f;
                    vecTempScale.y *= 1.4f;
                    _goShadow.transform.localScale = vecTempScale;

                }
                else
                {
                    m_bShadowBegin = true;
                }
            }
            */
        }

    }

 
}
