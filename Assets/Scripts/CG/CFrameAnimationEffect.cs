﻿/*
 *   “帧动画”组件 
 
 做技能特效的流程：
 1、BeginEffect
 2、EffectLoop: player执行effectLoop, EffectLopp内部内容
 3、RebornInit
 4、EndEffect




*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CFrameAnimationEffect : MonoBehaviour {

    public Sprite[] m_aryFrameSprites;


    public Image _imgMain; // 用在UI上
    public SpriteRenderer _sprMain; // 用在非UI上

    public bool m_bUI = true;

    public float m_fFrameInterval = 0f;
    float m_fTimeCount = 0f;

    bool m_bPlaying = false;
    int m_nFrameIndex = 0;
    public bool m_bLoop = false;
    public bool m_bReverse = false;
    bool m_bCurDir = true;
    public float m_fLoopInterval = 0f;
    bool m_bWaiting = false;
    float m_fWaitingTimeCount = 0f;

    public bool m_bPlayAuto = false;

    int m_nLoopTimes = 0;

    public bool m_bHideWhenEnd = true;
    public bool m_bDestroyWhenEnd = false;

    int m_nId = 0;

    // Use this for initialization
    void Start () {
		if (m_bPlayAuto)
        {
            BeginPlay(m_bLoop);
        }
	}
	
    public void SetId( int nId )
    {
        m_nId = nId;
    }

    // Update is called once per frame
    void Update () {
        Playing();
        LoopIntervalWaiting();
    }

    public void SetVisible( bool bVisible )
    {
        if (m_bUI)
        {
            _imgMain.gameObject.SetActive(bVisible);
        }
        else
        {
            _sprMain.gameObject.SetActive(bVisible);
        }
    }

    public void SetColor( Color Color)
    {
        if (_sprMain != null)
        {
            _sprMain.color = Color;
        }
    }

    public void SetSprite( Sprite sprite )
    {
        if (m_bUI)
        {
            _imgMain.sprite = sprite;
        }
        else
        {
            _sprMain.sprite = sprite;
        }
    }

    public void SetFrameInterval( float fFrameInterval )
    {
        m_fFrameInterval = fFrameInterval;
    }

    public void SetFrameAnimationEffect( Color color )
    {
        if ( _imgMain != null )
        {
            _imgMain.color = color;
        }

        if ( _sprMain != null )
        {
            _sprMain.color = color;
        }
    }

    public void SetReverse( bool bReverse )
    {
        m_bReverse = bReverse;
    }

    public void BeginPlay( bool bLoop, float fLoopInterval = 0f, bool bDestroyWhenEnd = false )
    {


        //_imgMain.gameObject.SetActive( true );
        SetVisible( true );

    
        m_bLoop = bLoop;
        m_nFrameIndex = 0;
        if ( m_bReverse && !m_bCurDir )
        {
            m_nFrameIndex = GetTotalFrameCount() - 1;
        }
        m_bPlaying = true;
        m_fTimeCount = 0f;

        m_fTimeCount = m_fFrameInterval;
        Playing();
    }

    int GetTotalFrameCount()
    {
        return m_aryFrameSprites.Length;
    }

    public int GetLoopTimes()
    {
        return m_nLoopTimes;
    }

    public void SetLoopTimes( int val )
    {
        m_nLoopTimes = val;
    }


    public void EndPlay()
    {
        m_bCurDir = !m_bCurDir;

        SetFrame(0);
        m_bPlaying = false;
        if (m_bLoop)
        {
            if (m_fLoopInterval > 0)
            {
                BeginLoopInterval();
            }
            else
            {
                BeginPlay(true);
            }
        }
        else
        {
            if (m_bHideWhenEnd)
            {
                SetVisible(false);
            }

            if (m_bDestroyWhenEnd)
            {
          //      CEffectManager.s_Instance.DeleteEffect( this );
            }
        }
    }

    public bool isEnd()
    {
        return !m_bPlaying;
    }

    void BeginLoopInterval()
    {
        m_bWaiting = true;
        m_fWaitingTimeCount = 0f;
        SetVisible( false );
    }

    void LoopIntervalWaiting()
    {
        if ( !m_bWaiting)
        {
            return;
        }
        m_fWaitingTimeCount += Time.deltaTime;
        if (m_fWaitingTimeCount <m_fLoopInterval)
        {
            return;
        }
        m_fWaitingTimeCount = 0f;
        EndLoopInterval();
    }

    void EndLoopInterval()
    {
        m_bWaiting = false;
        BeginPlay( true, m_fLoopInterval);
    }

    void Playing()
    {
        if ( !m_bPlaying)
        {
            return;
        }

        bool bEnd = false;

        if ( m_bReverse && !m_bCurDir )
        {
            if (m_nFrameIndex < 0)
            {
                bEnd = true;
            }
        }
        else
        {
            if (m_nFrameIndex >= m_aryFrameSprites.Length)
            {
                bEnd = true;
            }
        }
       

        if ( bEnd )
        {
            m_nLoopTimes++;
            EndPlay();
            return;
        }

        m_fTimeCount += Time.deltaTime;
        if (m_fTimeCount < m_fFrameInterval)
        {
            return;
        }
        m_fTimeCount = 0f;


        //_imgMain.sprite = m_aryFrameSprites[m_nFrameIndex++];

        SetSprite(m_aryFrameSprites[m_nFrameIndex] );
        if (m_bReverse && !m_bCurDir)
        {
            m_nFrameIndex--;
        }
        else
        {
            m_nFrameIndex++;
        }
    }

    void SetFrame( int nFrameIndex )
    {
        //_imgMain.sprite = m_aryFrameSprites[nFrameIndex];
        SetSprite(m_aryFrameSprites[nFrameIndex]);

       
    }
}
