﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour {

    public static DataManager s_Instance = null;

    public bool m_bTestConfig = false;
    public static string url = "";
    public static string url_formal = "http://39.108.53.61:5566/data/config1/";
    public static string url_test = "http://39.108.53.61:5566/data/config_test/";

    public const double LOAD_MY_DATA_INVALID_VALUE = -1d;
    public const string LOAD_MY_DATA_INVALID_STRING = "";

    // 一些常量
    public int MAX_PLANET_NUM = 3;
    public int MAX_SKILL_POINT_NUM = 3;
    public int MAX_TRACK_NUM_OF_PLANET = 5;

    // 交通工具
    Dictionary<string, int> m_dicCoinGainPerRound = new Dictionary<string, int>(); // 每走完一圈获得金币
    Dictionary<string, int> m_dicCoinVehiclePrice = new Dictionary<string, int>(); // 交通工具的售价
    float[] m_aryRoundTimeByLevel = new float[40]; // 交通工具绕一圈的时间


    Dictionary<string, int> m_dicCoinCostToPrestige = new Dictionary<string, int>(); // 每个赛道的重生价格

    Dictionary<string, sAutomobileConfig> m_dicAutomobileConfig = new Dictionary<string, sAutomobileConfig>();
    Dictionary<int, sPlanetConfig> m_dicPlanetConfig = new Dictionary<int, sPlanetConfig>();
    Dictionary<string, sTrackConfig> m_dicTrackConfig = new Dictionary<string, sTrackConfig>();

    Dictionary<string, string> m_dicTrackAndLevel2ResId = new Dictionary<string, string>();

    public enum eMoneyType
    {
        legal_currency, // 法币
        diamond,        // 钻石
        planet_0_coin,  // 青铜星金币
        planet_1_coin,  // 白银星金币
        planet_2_coin,  // 黄金星金币
    }; 

    // 载具配置
    public struct sAutomobileConfig
    {
        public int nId;
        public int nLevel; // 本交通工具的等级
        public int nResId;
        public string szName;
        public eMoneyType eCoinType; // 召唤价格（起始价格） - 金币类型 0 - 青铜星 1 - 白银星  2 - 黄金星
        public double nStartPrice_CoinValue; // 召唤价格（起始价格） - 金币
        public float fPriceRaisePercentPerBuy; // 每次购买之后价格上涨百分比
        public int nPriceDiamond;  // 购买价格 - 钻石(购买之后价格不增加)
        public int nCanUnlockLevel; // 可解锁等级
        public string szSuitableTrackId; // 可适配的赛道Id
        public double nBaseGain;  // 基础收益：跑完一圈赚多少金币
        public float fBaseSpeed; // 跑完一圈需要多少秒
        public int nDropLevel; // 掉落等级。解锁到这一级时，可以掉落第几级的载具
        public int nDropInterval; // 掉落的时间间隔

    }; // end sAutomobileConfig
    sAutomobileConfig tempAutomobileConfig;

    // 星球配置
    public struct sPlanetConfig
    {
        public string szName;    // 名称 
        public int nId;          // 唯一Id
        public double nUnlockPrice; // 解锁价格
        public eMoneyType eUnlockMoneyType; // 解锁所需的货币类型
    };
    sPlanetConfig tempPlanetConfig;


    // 赛道配置
    public struct sTrackConfig
    {
        public int nId;       // 赛道的Id
        public string szName; // 赛道的名字
        public int nPlanetId; // 所属星球Id
        public double nUnlockPrice;// 解锁本赛道所需花费的金币数量
        public float fEarning; // 基础收益倍数
        public double nBuyAdminBasePrice; // 招募主管的基础价格（即起始价格)

    };
    sTrackConfig tempTrackConfig;

    // 重生配置
    public struct sPrestigeConfig
    {
        public List<double> aryCoinCost;   // 金币消耗
        public List<float> aryCoinPromote; // 金币收益提升
    };
    sPrestigeConfig tempPrestigeConfig;
    Dictionary<string, sPrestigeConfig> m_dicPrestigeConfig = new Dictionary<string, sPrestigeConfig>();


    private void Awake()
    {
        s_Instance = this;

        if (m_bTestConfig)
        {
            url = url_test;
        }
        else
        {
            url = url_formal;
        }


        LoadMyData();
    }

    // Use this for initialization
    void Start () {
       
        


        Init();


      
      
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public int PlanetId2CoinId( int nPlanetId  )
    {
        int nCoinId = (int)eMoneyType.planet_0_coin;
        switch (nPlanetId)
        {
            case 0:
                {
                    nCoinId = (int)eMoneyType.planet_0_coin;
                }
                break;
            case 1:
                {
                    nCoinId = (int)eMoneyType.planet_1_coin;
                }
                break;
            case 2:
                {
                    nCoinId = (int)eMoneyType.planet_2_coin;
                }
                break;
        } // swith

        return nCoinId;
    }

    IEnumerator LoadConfig_Planet(string szFileName)
    {
        WWW www = new WWW(szFileName);
        yield return www; // 等待下载

        string[] aryLines = www.text.Split('\n');
        for (int i = 1; i < aryLines.Length; i++)
        {
            string[] aryParams = aryLines[i].Split(',');
            sPlanetConfig config = new sPlanetConfig();

            int nId = 0;
            if (!int.TryParse(aryParams[0], out nId))
            {
                continue;
            }

            config.nId = nId;
            config.szName = aryParams[1];
            config.nUnlockPrice = double.Parse(aryParams[2]);

            m_dicPlanetConfig[config.nId] = config;
        }

    } // end LoadConfig_Planet


    IEnumerator LoadConfig_Track(string szFileName)
    {
        WWW www = new WWW(szFileName);
        yield return www; // 等待下载
        string[] aryLines = www.text.Split('\n');
        for (int i = 1; i < aryLines.Length; i++)
        {
            string[] aryParams = aryLines[i].Split(',');
            sTrackConfig config = new sTrackConfig();

            int nId = 0;
            if (!int.TryParse(aryParams[0], out nId))
            {
                continue;
            }

            config.nId = nId;
            int nColIndex = 1;
            config.szName = aryParams[nColIndex++];
            config.nPlanetId = int.Parse(aryParams[nColIndex++]);
            config.nUnlockPrice = double.Parse(aryParams[nColIndex++]);
            config.fEarning = float.Parse(aryParams[nColIndex++]);
            config.nBuyAdminBasePrice = double.Parse(aryParams[nColIndex++]);

            string szPrestigeConfig = aryParams[nColIndex++] ;

            string szKey = config.nPlanetId + "_" + config.nId;

            ParsePrestigeConfig(szKey, szPrestigeConfig);

            m_dicTrackConfig[szKey] = config;
        } // end for i

        // poppin to do 写在这里很不规范，稍后修改一下
        MapManager.s_Instance.NewZengShouCounter(0, 0);
       

        m_bTrackConfigLoaded = true;
        TryLoadMyDataCurTrackPlanesData();

    } // end LoadConfig_Track



    void ParsePrestigeConfig( string szKey, string szContent ){
        
        sPrestigeConfig config = new sPrestigeConfig();
        config.aryCoinCost = new List<double>();
        config.aryCoinPromote = new List<float>();
        string[] aryParams = szContent.Split( '_' );
        for (int i = 0; i < aryParams.Length; i++ )
        {
            string[] arySubParams = aryParams[i].Split( ':' );

            double dCoinCost = 0;
            float fCoinPromote = 0;
            if ( !double.TryParse(arySubParams[0], out dCoinCost) )
            {
                Debug.LogError( "error" );
            }
            if (!float.TryParse(arySubParams[1], out fCoinPromote))
            {
                Debug.LogError("error");
            }

            config.aryCoinCost.Add(dCoinCost);
            config.aryCoinPromote.Add(fCoinPromote);
        }

        m_dicPrestigeConfig[szKey] = config;

    }

    public float GetPrestigePromote( int nPlanetId, int nTrackId, int nCurPrestigeTimes  )
    {
        sPrestigeConfig config = GetPrestigeConfig(nPlanetId + "_" + nTrackId);
        if (nCurPrestigeTimes == 0)
        {
            return 0;
        }
        else
        {
            return config.aryCoinPromote[nCurPrestigeTimes - 1];
        }
    }

    public sPrestigeConfig GetPrestigeConfig( string szKey )
    {
        if ( !m_dicPrestigeConfig.TryGetValue( szKey, out tempPrestigeConfig ) )
        {
            Debug.LogError( "error" );
        }

        return tempPrestigeConfig;
    }

    IEnumerator LoadConfig_Automobile(string szFileName)
    {         WWW www = new WWW(szFileName);         yield return www; // 等待下载
        string[] aryLines =  www.text.Split( '\n' );
        for (int i = 1; i < aryLines.Length; i++ )
        {
            string[] aryParams = aryLines[i].Split( ',' );
            sAutomobileConfig config = new sAutomobileConfig();

            int nId = 0;
            if (!int.TryParse(aryParams[0], out nId))
            {
                continue;
            }

            config.nId = nId;
            config.szName = aryParams[1];

            string szResId = aryParams[2];
          //  config.nResId = int.Parse(aryParams[2]);
            config.nStartPrice_CoinValue = 0;
            if ( !double.TryParse(aryParams[3], out config.nStartPrice_CoinValue ) )
            {
                config.nStartPrice_CoinValue = 0;
            }
            config.fPriceRaisePercentPerBuy = float.Parse(aryParams[4]);
           // config.nStartPrice_CoinType = int.Parse(aryParams[5]);

           

            config.nPriceDiamond = int.Parse(aryParams[5]);
            config.nLevel = int.Parse(aryParams[6]);
            config.nCanUnlockLevel = int.Parse(aryParams[7]);
            config.szSuitableTrackId = aryParams[8];

            //  购买交通工具所需的金币类型，不用配置，而是根据该交通工具适用的星球来决定的
            string[] aryTemp = config.szSuitableTrackId.Split( '_' );
            int nPlanetId = int.Parse(aryTemp[0]);
            switch(nPlanetId)
            {
                case 0:
                    {
                        config.eCoinType = eMoneyType.planet_0_coin;
                    }
                    break;
                case 1:
                    {
                        config.eCoinType = eMoneyType.planet_1_coin;
                    }
                    break;
                case 2:
                    {
                        config.eCoinType = eMoneyType.planet_2_coin;
                    }
                    break;
            }// end switch


            config.nBaseGain = 0;//
            if ( !double.TryParse(aryParams[9], out config.nBaseGain))
            {
                config.nBaseGain = 0;
            }
            config.fBaseSpeed = float.Parse(aryParams[10]);

            config.nDropLevel = 1;
            if ( aryParams.Length > 11 )
            {
                if ( !int.TryParse(aryParams[11], out config.nDropLevel ))
                {
                   config.nDropLevel = 1;
                }
            }


            config.nDropInterval = 10;
            if (aryParams.Length > 12)
            {
                if (!int.TryParse(aryParams[12], out config.nDropInterval))
                {
                    config.nDropInterval = 10;
                }
            }


            string szResIdKey = config.szSuitableTrackId + "_" + config.nLevel;
            m_dicTrackAndLevel2ResId[szResIdKey] = szResId;

            string szKey = config.szSuitableTrackId + "_" + config.nLevel;
            m_dicAutomobileConfig[/*config.nId*/szKey] = config;
        }

        // poppin to do
        // 载具配置加载完成之后，开始读“当前所处赛道上所有汽车的状况”的存档
        // 注意，正规的做法应该是注册回调事件。当前进度太紧就直接写在这里了
        // MapManager.s_Instance.LoadMyData_CurTrackPlanesData();
        m_bAutomobileConfigLoaded = true;
        TryLoadMyDataCurTrackPlanesData();
    }

    bool m_bAutomobileConfigLoaded = false;
    bool m_bTrackConfigLoaded = false;
    void TryLoadMyDataCurTrackPlanesData()
    {
        Debug.Log( "try......" + m_bAutomobileConfigLoaded + "_" + m_bTrackConfigLoaded);
        if (m_bAutomobileConfigLoaded && m_bTrackConfigLoaded)
        {
            MapManager.s_Instance.LoadMyData_CurTrackPlanesData();
        }
    }

    public Dictionary<string, sAutomobileConfig> GetAutomobileConfig()
    {
        return m_dicAutomobileConfig;
    }


    public void Init()
    {
        string szConfigFileName_Automobile = url + "automobile.csv";
        string szConfigFileName_Planet = url + "planet.csv";
        string szConfigFileName_Track = url + "track.csv";
        StartCoroutine( LoadConfig_Automobile (szConfigFileName_Automobile));
        StartCoroutine(LoadConfig_Planet(szConfigFileName_Planet));
        StartCoroutine(LoadConfig_Track(szConfigFileName_Track));


        // 交通工具每走一圈所获得的金币
        for (int i = 0; i < 3; i++ ) // planet
        {
            for (int j = 0; j < 5; j++ ) // district
            {
                for (int k = 1; k <= 40;  k++ ) // level
                {
                    int nValue = k  * ( j + 1 ) * ( i + 1 );
                    nValue *= 100;
                    string szKey = i + "_" + j + "_" + k;
                    m_dicCoinGainPerRound[szKey] = nValue;
                } // end for k
            } // end for j
        } // end for i

        // 交通工具的售价
        for (int i = 0; i < 3; i++) // planet
        {
            for (int j = 0; j < 5; j++) // district
            {
                for (int k = 1; k <= 40; k++) // level
                {
                    int nValue = k * (j + 1) * (i + 1) * 100;
                    string szKey = i + "_" + j + "_" + k;
                    m_dicCoinVehiclePrice[szKey] = nValue;
                } // end for k
            } // end for j
        } // end for i



        // 每一个赛道重生所需要的金币
        for (int i = 0; i < 3; i++) // planet
        {
            for (int j = 0; j < 5; j++) // district
            {
                    int nValue = 10000 * (j + 1) * (i + 1);
                    string szKey = i + "_" + j;
                    m_dicCoinCostToPrestige[szKey] = nValue;
            } // end for j
        } // end for i

        for (int i = 0; i < 40; i++)
        {
            m_aryRoundTimeByLevel[i] = 6f - 0.2f * i;
        }
        // poppin to discuss
        // 随着重生次数增加，同一个赛道的重生花费应该不同吧？？

        // poppin to discuss 
        // 不同的星球和赛道，重生之后的收益数值是否应该不同


    }

    // right here
    // 获取：交通工具每走一圈所获得的金币
    public double GetCoinGainperRound( int nPlanetId, int nDistrictId, int nLevel )
    {

        /*
        string szKey = nPlanetId + "_" + nDistrictId + "_" + nLevel;
     
        if ( !m_dicCoinGainPerRound.TryGetValue( szKey, out nValue ) )
        {
            nValue = 0;
        }
        */
        double nAutomobileEarning = DataManager.s_Instance.GetAutomobileConfig(nPlanetId, nDistrictId, nLevel).nBaseGain;
        float fTrackRaise = DataManager.s_Instance.GetTrackConfigById(nPlanetId, nDistrictId).fEarning;
        nAutomobileEarning =  nAutomobileEarning * fTrackRaise ;

        return nAutomobileEarning;
    }

    // 获取：交通工具的售价
    public int GetVehiclePrice(int nPlanetId, int nDistrictId, int nLevel)
    {
        string szKey = nPlanetId + "_" + nDistrictId + "_" + nLevel;
        int nValue = 0;
        if (!m_dicCoinVehiclePrice.TryGetValue(szKey, out nValue))
        {
            nValue = 0;
        }

        // 根据购买次数，价格会增加
        Planet planet = MapManager.s_Instance.GetPlanetById(nPlanetId);
        District district = planet.GetDistrictById(nDistrictId);
        int nBuyTimes = district.GetVehicleBuyTimes(nLevel);

        // 公式是瞎鸡巴乱填的，后期数值策划来弄 
        if (nBuyTimes > 0)
        {
            nValue *= ( nBuyTimes + 1 );
        }


        return nValue;
    }

    public float GetVehicleRunTimePerRound( int nLevel )
    {
        return GetAutomobileConfig(MapManager.s_Instance.GetCurPlanet().GetId(), MapManager.s_Instance.GetCurDistrict().GetId(), nLevel).fBaseSpeed;
       //int nIndex = nLevel;
       // return m_aryRoundTimeByLevel[nIndex];
    }

    public int GetPrestigaeCoinCost(int nPlanetId, int nDistrictId )
    {
        string szKey = nPlanetId + "_" + nDistrictId;
        int nValue = 0;

        if (!m_dicCoinCostToPrestige.TryGetValue(szKey, out nValue))
        {
            nValue = 0;
        }
        return nValue;
    }
   
    public int GetPrestigeGain(int nPlanetId, int nDistrictId, int nCurPrestigeTimes)
    {
        if (nCurPrestigeTimes == 0)
        {
            return 1;
        }
        return nCurPrestigeTimes * 2;
    }

    public double GetPlanetUnlockCostById( int nPlanetId )
    {
        if ( !m_dicPlanetConfig.TryGetValue( nPlanetId, out tempPlanetConfig) )
        {
            Debug.LogError(" !m_dicPlanetConfig.TryGetValue( nPlanetId, out nCost )");
        }

        return tempPlanetConfig.nUnlockPrice;
    }

    // 根据星球的Id号获取该星球的配置数值
    public sPlanetConfig GetPlanetConfigById( int nPlanetId )
    {
        if (!m_dicPlanetConfig.TryGetValue(nPlanetId, out tempPlanetConfig))
        {
            Debug.LogError(" !m_dicPlanetConfig.TryGetValue( nPlanetId, out nCost )");
        }

        return tempPlanetConfig;
    }

    // 根据赛道的Id号获取该赛道的配置数值
    public sTrackConfig GetTrackConfigById(int nPlanetId, int nTrackId )
    {
        string szKey = nPlanetId + "_" + nTrackId;
        if (!m_dicTrackConfig.TryGetValue(szKey, out tempTrackConfig))
        {
           // Debug.LogError(" !m_dicPlanetConfig.TryGetValue( nPlanetId, out nCost )");
        }

        return tempTrackConfig;
    }

    // 根据星球Id、赛道Id、载具等级，获取载具配置
    public sAutomobileConfig GetAutomobileConfig( int nPlanetId, int nTrackId, int nAutoLevel )
    {
        string szKey = nPlanetId + "_" + nTrackId + "_" + nAutoLevel;
        if ( !m_dicAutomobileConfig.TryGetValue(szKey, out tempAutomobileConfig) )
        {
            Debug.LogError("GetAutomobileConfig");
        }

        return tempAutomobileConfig;
    }

    /////////////////// 存档系统 /////////////////
    /*
    Coin      金币
    Diamond   钻石
    Point     技能点(用于解锁和升级天赋点)

    */
    public static bool IsDataValid( string szData )
    {
        if (szData == "")
        {
            return false;
        }

        return true;
    }

    Dictionary<string, string> m_dicLoadMyData_String = new Dictionary<string, string>();
    Dictionary<string, double> m_dicLoadMyData = new Dictionary<string, double>();
    public void LoadMyData()
    {
        return;

        Debug.Log("===================== 读档 ==========================");


        string szKey = "";
        string szData = "";
        string[] aryParams = null;
        double dVal = 0;
        List<double> lstDouble = new List<double>();

        // 每个星球的金币
        for (int i = 0; i < MAX_PLANET_NUM; i++)
        {
            szData = PlayerPrefs.GetString("Coin" + i);
            if (IsDataValid(szData))
            {
                dVal = 0;
                double.TryParse(szData, out dVal);
                m_dicLoadMyData["Coin" + i] = dVal;

            }
        }

        // 钻石
        szData = PlayerPrefs.GetString("Diamond");
        if (IsDataValid(szData))
        {
            double.TryParse(szData, out dVal);
            m_dicLoadMyData["Diamond"] = dVal;
        }

        // 
        // 每个星球的技能点 
        for (int i = 0; i < MAX_PLANET_NUM; i++)
        {
            szData = PlayerPrefs.GetString("SkillPoint" + i);
            if (IsDataValid(szData))
            {
                dVal = 0;
                double.TryParse(szData, out dVal);
                m_dicLoadMyData["SkillPoint" + i] = dVal;

            }
        }

        // 星球解锁情况
        for (int i = 0; i < MAX_PLANET_NUM; i++)
        {
            szData = PlayerPrefs.GetString("PlanetUnlock" + i);
            if (IsDataValid(szData))
            {
                dVal = 0;
                double.TryParse(szData, out dVal);
                m_dicLoadMyData["PlanetUnlock" + i] = dVal;

            }
        }

        // 赛道的解锁情况
        for (int i = 0; i < MAX_PLANET_NUM; i++)
        {
            for (int j = 0; j < MAX_TRACK_NUM_OF_PLANET; j++)
            {
                szKey = "TrackUnlock" + i + "_" + j;
                szData = PlayerPrefs.GetString(szKey);
                if (IsDataValid(szData))
                {
                    dVal = 0;
                    double.TryParse(szData, out dVal);
                    m_dicLoadMyData[szKey] = dVal;

                }
            }
        }

        // 当前所处的星球和赛道
        szKey = "CurPlanetAndTrack";
        szData = PlayerPrefs.GetString(szKey);
        m_dicLoadMyData_String[szKey] = szData;

        // 当前所有赛道上的载具实时信息
        for (int i = 0; i < MAX_PLANET_NUM; i++)
        {
            for (int j = 0; j < MAX_TRACK_NUM_OF_PLANET; j++)
            {
                szKey = "CurTrackPlanesData" + i + "_" + j;
                szData = PlayerPrefs.GetString(szKey);
                m_dicLoadMyData_String[szKey] = szData;
            } // end j
        } // end i





    } // end LoadMyData()

    // 清除存档
    public void ClearMyData()
    {
        m_bCanSave = false;


        string szKey = "";
        string szData = "";

        // 每个星球的金币、技能点
        for (int i = 0; i < MAX_PLANET_NUM; i++)
        {
            PlayerPrefs.SetString("Coin" + i, LOAD_MY_DATA_INVALID_VALUE.ToString());
            PlayerPrefs.SetString("SkillPoint" + i, LOAD_MY_DATA_INVALID_VALUE.ToString());
        }

        // 钻石
        PlayerPrefs.SetString("Diamond", LOAD_MY_DATA_INVALID_VALUE.ToString());

        // 星球解锁情况、赛道解锁情况
        for (int i = 0; i < MAX_PLANET_NUM; i++)
        {
            PlayerPrefs.SetString("PlanetUnlock" + i, LOAD_MY_DATA_INVALID_VALUE.ToString());
            for (int j = 0; j < MAX_TRACK_NUM_OF_PLANET; j++)
            {
                PlayerPrefs.SetString("TrackUnlock" + i + "_" + j,  LOAD_MY_DATA_INVALID_VALUE.ToString());
            } // end j
        } // end i

        // 当前所处赛道
        szKey = "CurPlanetAndTrack";
        PlayerPrefs.SetString(szKey, LOAD_MY_DATA_INVALID_STRING);

        // 当前所有赛道上的载具实时信息
        for (int i = 0; i < MAX_PLANET_NUM; i++)
        {
            for (int j = 0; j < MAX_TRACK_NUM_OF_PLANET; j++)
            {
                szKey = "CurTrackPlanesData" + i + "_" + j;
                PlayerPrefs.SetString(szKey, LOAD_MY_DATA_INVALID_STRING);
            } // end j
        } // end i





        UIMsgBox.s_Instance.ShowMsg( "存档已清除" );


    } // end ClearMyData

    public double GetMyData( string szKey )
    {
        double dValue = LOAD_MY_DATA_INVALID_VALUE;

        if ( !m_dicLoadMyData.TryGetValue( szKey, out dValue) )
        {
            dValue = LOAD_MY_DATA_INVALID_VALUE;
        }

        return dValue;
    }

    public string GetMyData_String(string szKey )
    {
        string szData = LOAD_MY_DATA_INVALID_STRING;

        if ( !m_dicLoadMyData_String.TryGetValue(szKey, out szData) )
        {
            szData = LOAD_MY_DATA_INVALID_STRING;
        }

        return szData;
    }

    bool m_bCanSave = true;
    public void SaveMyData( string szKey, double dValue )
    {
        if ( !m_bCanSave)
        {
            return;
        }

        PlayerPrefs.SetString(szKey, dValue.ToString());
    }

    public void SaveMyData(string szKey,string szValue )
    {
        if (!m_bCanSave)
        {
            return;
        }

        PlayerPrefs.SetString(szKey, szValue);
    }

    public void SaveCurTrackPlanesData()
    {
        string szKey = "CurTrackPlanesData" + MapManager.s_Instance.GetCurPlanet().GetId() + "_" + MapManager.s_Instance.GetCurDistrict().GetId();
        DataManager.s_Instance.SaveMyData(szKey, Main.s_Instance.GenerateData());
    }

    /////////////// end 存档系统 /////////////////



} // end class
