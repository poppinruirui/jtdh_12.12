﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartBelt : MonoBehaviour {
    
    public static StartBelt s_Instance = null;
    public StartRunArrow[] m_aryStartArrows;

    int m_nMaxNum = 10;
    int m_nCurNum = 0;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetRunningPlanesNum( int nNum )
    {
        m_nCurNum = nNum;

        Main.s_Instance._txtNumOfRunningPlane.text = m_nCurNum + "/10";

        for (int i = 0; i < m_aryStartArrows.Length; i++)
        {
            StartRunArrow arrow = m_aryStartArrows[i];
            arrow.TurnOnOrOff(false);
        }

        for (int i = 0; i < m_aryStartArrows.Length; i++ )
        {
            StartRunArrow arrow = m_aryStartArrows[i];
            if ( i < nNum )
            {
                arrow.TurnOnOrOff(true);
            }
            else
            {
                arrow.TurnOnOrOff(false);
            }

        }

    }

    public int GetMaxNum()
    {
        return m_nMaxNum;
    }

    public bool CheckIfCanRun()
    {
        return m_nCurNum < m_nMaxNum;
    }


}
